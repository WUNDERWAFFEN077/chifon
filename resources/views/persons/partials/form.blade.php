<!-- form start -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>                    
    </div>
@endif
<div class="form-group col-md-6">
  {{ Form::label('name', 'Tipo de documento:', ['class' => 'col-sm-4 control-label required']) }}
    <div class="col-sm-8">
    <select class="form-control input-sm" name="identification_code">
            @foreach ($identifications as $identification)
            <option value="{{ $identification->code }}" @if($identification->code==old('identification_code',$person->identification_code)) selected @endif>{{ $identification->name }}</option>
            @endforeach
        </select>
    </div> 
</div>
<div class="form-group col-md-6">
  {{ Form::label('identification', 'Doc. Identidad:', ['class' => 'col-sm-4 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('identification', null, ['class' => 'form-control input-sm', 'id' => 'identification']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('full_name', 'Nombre:', ['class' => 'col-sm-4 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('full_name', null, ['class' => 'form-control input-sm', 'id' => 'full_name']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('primary_last_name', 'Ap. Paterno:', ['class' => 'col-sm-4 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('primary_last_name', null, ['class' => 'form-control input-sm', 'id' => 'primary_last_name']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('second_last_name', 'Ap. Materno:', ['class' => 'col-sm-4 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('second_last_name', null, ['class' => 'form-control input-sm', 'id' => 'second_last_name']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('address', 'Dirección:', ['class' => 'col-sm-4 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('address', null, ['class' => 'form-control input-sm', 'id' => 'address']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('ubigeo_code', 'Ubigeo:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('ubigeo_code', null, ['class' => 'form-control input-sm', 'id' => 'ubigeo_code']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('gender', 'Sexo:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::radio('gender', 'M',(old('gender',$person->gender) ==  'M')) }}
  {{ Form::label('','Hombre',['class' => 'input-sm']) }}

  {{ Form::radio('gender', 'F',(old('gender',$person->gender) ==  'F')) }}
  {{ Form::label('','Mujer',['class' => 'input-sm']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('email', 'Correo:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('email', null, ['class' => 'form-control input-sm', 'id' => 'email']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('phone1', 'Teléfono 1:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('phone1', null, ['class' => 'form-control input-sm', 'id' => 'phone1']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('phone2', 'Teléfono 2:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('phone2', null, ['class' => 'form-control input-sm', 'id' => 'phone2']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('fax', 'Fax:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('fax', null, ['class' => 'form-control input-sm', 'id' => 'fax']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('mobile', 'Celular:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('mobile', null, ['class' => 'form-control input-sm', 'id' => 'mobile']) }}
  </div>
</div>
<div class="form-group col-md-6">
  {{ Form::label('observations', 'Observaciones:', ['class' => 'col-sm-4 control-label not-required']) }}
  <div class="col-sm-8">
  {{ Form::text('observations', null, ['class' => 'form-control input-sm', 'id' => 'observations']) }}
  </div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-8">
		<a  type="button" class='btn btn-default' name='cancel' id='cancel' href="{{ route('persons.index') }}">Cancelar</a>
		{{ Form::submit('Guardar', ['class' => 'btn bg-olive']) }}
	</div>
</div>


