<!-- form start -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>                    
    </div>
@endif
<div class="form-group">
  {{ Form::label('name', 'Nombre:', ['class' => 'col-sm-2 control-label required']) }}
  <div class="col-sm-4">
  {{ Form::text('name', null, ['class' => 'form-control input-sm', 'id' => 'name']) }}
  </div>

  {{ Form::label('reference_code', 'Referencia:', ['class' => 'col-sm-2 control-label not-required']) }}
  <div class="col-sm-4">
  {{ Form::text('reference_code', null, ['class' => 'form-control input-sm', 'id' => 'reference_code']) }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('measure_code', 'Unidad de  medida:', ['class' => 'col-sm-2 control-label required']) }}
    <div class="col-sm-4">
    <select class="form-control input-sm" name="measure_code">
            @foreach ($measures as $measure)
            <option value="{{ $measure->code }}" @if($measure->code==old('measure_code',$product->measure_code)) selected @endif>{{ $measure->name }}</option>
            @endforeach
        </select>
    </div> 
  {{ Form::label('category_id', 'Categoria:', ['class' => 'col-sm-2 control-label required']) }}
    <div class="col-sm-4">
    <select class="form-control input-sm" name="category_id">
            @foreach ($categories as $category)
            <option value="{{ $category->id }}" @if($category->code==old('category_code',$product->category_id)) selected @endif>{{ $category->name }}</option>
            @endforeach
        </select>
    </div> 
</div>
<div class="form-group">
  {{ Form::label('line_id', 'Linea:', ['class' => 'col-sm-2 control-label required']) }}
    <div class="col-sm-4">
        <select class="form-control input-sm" name="line_id">
            @foreach ($lines as $line)
            <option value="{{ $line->id }}" @if($line->id==old('line_id',$product->line_id)) selected @endif>{{ $line->name }}</option>
            @endforeach
        </select>
    </div>
  {{ Form::label('existence_code', 'Existencia:', ['class' => 'col-sm-2 control-label required']) }}
    <div class="col-sm-4">
      <select class="form-control input-sm" name="existence_code">
              @foreach ($existences as $existence)
              <option value="{{ $existence->code }}" @if($existence->code==old('existence_code',$product->existence_code)) selected @endif>{{ $existence->name }}</option>
              @endforeach
      </select>
    </div> 
</div>
<div class="form-group">
  {{ Form::label('description', 'Descripción:', ['class' => 'col-sm-2 control-label not-required']) }}
  <div class="col-sm-10">
  {{ Form::text('description', null, ['class' => 'form-control input-sm', 'id' => 'description']) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('long_description', 'Descripción Larga:', ['class' => 'col-sm-2 control-label not-required']) }}
  <div class="col-sm-10">
  {{ Form::textarea('long_description', null, ['rows' => 2,'class' => 'form-control input-sm', 'id' => 'long_description']) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('is_inventariable', 'Inventariable?:', ['class' => 'col-sm-2 control-label not-required']) }}
  <div class="col-sm-4">
  {{ Form::checkbox('is_inventariable', '1',(old('is_inventariable',$product->is_inventariable) ==  '1')) }}
  {{ Form::label('','&nbsp;',['class' => 'input-sm']) }}
  </div>
   {{ Form::label('tax_type', 'Impuesto:', ['class' => 'col-sm-2 control-label not-required']) }}
  <div class="col-sm-4">
  {{ Form::select('tax_type', [
   '10' => 'Gravado - (18%)',
   '20' => 'Exonerado - (0%)',
   '30' => 'Inafecto - (0%)'],
    null, ['class' => 'form-control input-sm']
    ) 
  }}
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-8">
    <a  type="button" class='btn btn-default' name='cancel' id='cancel' href="{{ route('products.index') }}">Cancelar</a>
    {{ Form::submit('Guardar', ['class' => 'btn bg-olive']) }}
  </div>
</div>


