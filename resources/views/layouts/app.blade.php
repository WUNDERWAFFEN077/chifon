<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Chifon | Página comercial</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }} ">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">  
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}} ">
  <!-- JqGrid -->
  <link rel="stylesheet" href="{{ asset('bower_components/jqgrid/jqgrid.min.css')}} ">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}} ">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css')}}">
  <!-- Noty -->
  <link rel="stylesheet" href="{{ asset('bower_components/noty/noty.css')}}">
  <link rel="stylesheet" href="{{ asset('bower_components/noty/animate.css')}}">
  <link rel="stylesheet" href="{{ asset('bower_components/noty/bootstrap-v3.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}} ">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">

  <!-- ap.js -->
  <!--<link rel="stylesheet" href="{{ asset('css/app.css')}}">-->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">


  <style>
    .invoice-items .price,.invoice-items .quantity{
      width: 90%;
    }
    .invoice .invoice-number{
      margin-left: 20px;
    }
    .invoice .invoice-number .number-info{
        border: 1px solid #f2f2f2 !important;
        color: #3e3e49 !important;
        font-size: 18px;
        font-family: Roboto;
        padding: 10px !important;
        text-align: left;
        border-radius: 4px;
    }
    .invoice .invoice-number label{
        font-size: 18px;
        font-family: Roboto;
    }

    .invoice header {
        border-bottom: 1px solid #DADADA;
        margin-bottom: 15px;
    }

    .invoice address {
        color: #7F8597;
        line-height: 1.5em;
    }

    .invoice .btn-group-sm>.btn,.invoice .btn-sm {
      padding: 3px 10px;
      font-size: 12px;
      line-height: 1.5;
      border-radius: 3px;
    }


    .invoice .form-group {
      margin-bottom: 7px;
    }
  </style>

  <style type="text/css">
    label.valid {
      background: white !important;
      width: 20px;
      height: 20px;
      position: absolute;
      margin-left: 0px;
      display: inline;
      border: 0 solid;
      display: inline-block;
      z-index: -1;
    }

    label.error {  
      background: url("{{ asset('images/icons/error.png')}}") center center no-repeat;
      width: 20px;
      height: 20px;
      position: absolute; 
      right: -7px; top: 0;
      margin-left: 0px;
      border: 0 solid;
      text-indent: -9999px;

    }
  </style>

  <style type="text/css">   
  /* weights */
  .text-weight-light {
    font-weight: 300;
  }

  .text-weight-normal {
    font-weight: 400;
  }

  .text-weight-semibold {
    font-weight: 600;
  }

  .text-weight-bold {
    font-weight: 700;
  }

  .text-uppercase {
    text-transform: uppercase;
  }

  .text-lowercase {
    text-transform: lowercase;
  }

  .text-capitalize {
    text-transform: capitalize;
  }

  .rounded {
    border-radius: 5px;
  }

  .b-thin {
    border-width: 3px;
  }

  .b-normal {
    border-width: 5px;
  }

  .b-thick {
    border-width: 7px;
  }

  
  /* Spacements */
/* spacement top & bottom */
.m-none {
  margin: 0 !important;
}

.m-auto {
  margin: 0 auto !important;
}

.m-xs {
  margin: 5px !important;
}

.m-sm {
  margin: 10px !important;
}

.m-md {
  margin: 15px !important;
}

.m-lg {
  margin: 20px !important;
}

.m-xl {
  margin: 25px !important;
}

.m-xlg {
  margin: 30px !important;
}

/* spacement top  */
.mt-none {
  margin-top: 0 !important;
}

.mt-xs {
  margin-top: 5px !important;
}

.mt-sm {
  margin-top: 10px !important;
}

.mt-md {
  margin-top: 15px !important;
}

.mt-lg {
  margin-top: 20px !important;
}

.mt-xl {
  margin-top: 25px !important;
}

.mt-xlg {
  margin-top: 30px !important;
}

/* spacement bottom */
.mb-none {
  margin-bottom: 0 !important;
}

.mb-xs {
  margin-bottom: 5px !important;
}

.mb-sm {
  margin-bottom: 10px !important;
}

.mb-md {
  margin-bottom: 15px !important;
}

.mb-lg {
  margin-bottom: 20px !important;
}

.mb-xl {
  margin-bottom: 25px !important;
}

.mb-xlg {
  margin-bottom: 30px !important;
}

/* spacement left */
.ml-none {
  margin-left: 0 !important;
}

.ml-xs {
  margin-left: 5px !important;
}

.ml-sm {
  margin-left: 10px !important;
}

.ml-md {
  margin-left: 15px !important;
}

.ml-lg {
  margin-left: 20px !important;
}

.ml-xl {
  margin-left: 25px !important;
}

.ml-xlg {
  margin-left: 30px !important;
}

/* spacement right  */
.mr-none {
  margin-right: 0 !important;
}

.mr-xs {
  margin-right: 5px !important;
}

.mr-sm {
  margin-right: 10px !important;
}

.mr-md {
  margin-right: 15px !important;
}

.mr-lg {
  margin-right: 20px !important;
}

.mr-xl {
  margin-right: 25px !important;
}

.mr-xlg {
  margin-right: 30px !important;
}

/* Spacement Padding */
.p-none {
  padding: 0 !important;
}

.p-xs {
  padding: 5px !important;
}

.p-sm {
  padding: 10px !important;
}

.p-md {
  padding: 15px !important;
}

.p-lg {
  padding: 20px !important;
}

.p-xl {
  padding: 25px !important;
}

.p-xlg {
  padding: 30px !important;
}

/* spacement top  */
.pt-none {
  padding-top: 0 !important;
}

.pt-xs {
  padding-top: 5px !important;
}

.pt-sm {
  padding-top: 10px !important;
}

.pt-md {
  padding-top: 15px !important;
}

.pt-lg {
  padding-top: 20px !important;
}

.pt-xl {
  padding-top: 25px !important;
}

.pt-xlg {
  padding-top: 30px !important;
}

/* spacement bottom */
.pb-none {
  padding-bottom: 0 !important;
}

.pb-xs {
  padding-bottom: 5px !important;
}

.pb-sm {
  padding-bottom: 10px !important;
}

.pb-md {
  padding-bottom: 15px !important;
}

.pb-lg {
  padding-bottom: 20px !important;
}

.pb-xl {
  padding-bottom: 25px !important;
}

.pb-xlg {
  padding-bottom: 30px !important;
}

/* spacement left */
.pl-none {
  padding-left: 0 !important;
}

.pl-xs {
  padding-left: 5px !important;
}

.pl-sm {
  padding-left: 10px !important;
}

.pl-md {
  padding-left: 15px !important;
}

.pl-lg {
  padding-left: 20px !important;
}

.pl-xl {
  padding-left: 25px !important;
}

.pl-xlg {
  padding-left: 30px !important;
}

/* spacement right  */
.pr-none {
  padding-right: 0 !important;
}

.pr-xs {
  padding-right: 5px !important;
}

.pr-sm {
  padding-right: 10px !important;
}

.pr-md {
  padding-right: 15px !important;
}

.pr-lg {
  padding-right: 20px !important;
}

.pr-xl {
  padding-right: 25px !important;
}

.pr-xlg {
  padding-right: 30px !important;
}

.ib {
  display: inline-block;
  vertical-align: top;
}

.va-middle {
  vertical-align: middle;
}

.ws-nowrap {
  white-space: nowrap;
}

.ws-normal {
  white-space: normal;
}


  




  /*Color Skins Nabvar*/
  .skin-green-light .main-header .navbar {
    background-color: #3d9970;
  }

  .skin-green-light .main-header .logo {
    background-color: #3d9970;
    color: #fff;
    border-bottom: 0 solid transparent;
  }

  .skin-green-light .main-header .logo:hover {
    background-color: #3d9970;
  }

  #radioBtn .notActive{
      color: #BDBDBD !important;
      background-color: #fff !important;
      border: 1px solid #BDBDBD;
  }

  /*Tooltip*/
  [class^="tooltip"] {
    position: relative; 
  }

  [class^="tooltip"]:after{
      min-width: 200px;
      font-size: 12px;
      opacity: 0;
      visibility: hidden;
      position: absolute;
      content: attr(data-tooltip);
      padding: 3px 10px;
      top: 1.4em;
      left: 50%;
      transform: translateX(-50%) translateY(-2px);
      background: #F2F2F2;
      color: #088A68;
      /*white-space: nowrap;*/
      z-index: 5;
      border: 1px solid #848484;
      border-radius: 2px;
      transition: opacity 0.2s cubic-bezier(.64,.09,.08,1), transform 0.2s cubic-bezier(.64,.09,.08,1);
  }

  [class^="tooltip"]:hover:after{
        display: block;
        opacity: 1;
        visibility: visible;
        transform: translateX(8%) translateY(0);
  }
  
  .tooltip--right:after {
    top: -4px;
    left: 100%;
    transform: translateX(10%) translateY(0);
  }

  .tooltip--righ:hover:after{
    transform: translateX(8%) translateY(0);
  }


  .icon-help {
    background-image: url(https://cdn1.alegra.com/images/sm-help.png) !important;
    background-position: center;
    background-repeat: no-repeat;
  }


   /*select2*/
  .select2-selection__rendered {
      line-height: 20px !important;
  }
  .select2-container{ width: 98% !important; }

  .select2-selection {
    height: 26px !important;
    border-radius: 3px !important;
    
  }

  .select2-selection__arrow {
    height: 18px !important;
    color: #555;
  }

  .select2-results__option.select2-results__message{
    font-size: 13px;
  }
  .select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field{
    border-color: #088A4B !important;
  }


 /*xs form input*/
  .form-control-xs {
      height: 26px;/*22px default*/
      padding: 5px 5px;
      font-size: 14px;
      line-height: 1.5;
      border-radius: 3px;

  }
  select.form-control-xs{
      padding: 0px 0px;
  }

  button.form-control-xs{
      padding: 0px 5px;
      font-size: 13px;
  }
  /*a.form-control-xs{
      padding: 0px 10px;
      font-size: 14px;
  }*/


  .form-control-xs.date{
    width: 85px;
    float: left !important;
  }

  .input-group-addon-xs{
    padding: 0px 5px;   
  }

  .control-label-xs { 
    padding-top: 0px;
    margin-bottom: 0;
    text-align: right;
    font-weight: 500;
    font-size: 12px;
    line-height: 1.5;
  }


  textarea { resize: none; }

  .form-group label {
      color: #767681;
      font-size: 14px;
  }
  .content-header {
      padding: 12px 20px;
      border-bottom: 1px solid #dddddd !important;
      background: #f7f7f8 !important;
      position: relative;
      margin: 0px;
  }

  form .form-group .control-label {
      padding-top: 5px;
      font-weight: 500;
  }

  label.not-required:after {
    color: transparent;
    content: "*";
    font-weight: bold;
    margin-left: 5px;
  }

  label.required:after {
    color: #37bc9b;
    content: "*";
    font-weight: bold;
    margin-left: 5px;
  }


    .btn_to_link{
      
      text-decoration: none !important;
      color: #777;
      width: 100%;
      text-align: left;
      padding: 2px 20px;
    }
    .btn_to_link:hover{
      text-decoration: none;
      color: #262626;
      background-color: #e7e7e7;
      margin-right: 10px;
    }

    .btn_to_link .fa {
      margin-right: 10px !important;
    }

    #navbar-search-input{
      background-color: #FAFAFA !important;
    }

    .ui-jqdialog .ui-widget-header {
        background-image: linear-gradient(to bottom, #FFF 0, #EEE 100%) !important;
        background-repeat: repeat-x !important;
        border-image: none !important;
        border-bottom: 1px solid #DDD !important;
        color: #669FC7 !important;
        min-height: 38px !important;
        position: relative !important;
    }

    .ui-dialog, .ui-jqdialog {
        z-index: 1050 !important;
        background-color: #FFF !important;
        padding: 0 !important;
        border: 1px solid #DDD !important;
        box-shadow: 0 2px 4px rgba(0, 0, 0, .2) !important;
    }

    .ui-dialog .ui-dialog-titlebar,
    .ui-dialog .ui-jqdialog-titlebar,
    .ui-jqdialog .ui-dialog-titlebar,
    .ui-jqdialog .ui-jqdialog-titlebar {
        background-color: #F1F1F1 !important;
        font-size: 16px !important;
        color: #669FC7 !important;
        padding: 0 !important;
    }

    .ui-jqdialog .ui-widget-header .ui-jqdialog-title {
        line-height: 38px !important;
        margin: 0 !important;
        padding: 0 0 0 12px !important;
        text-align: left !important;
    }


    .widget-header .ui-jqdialog-title {
        padding-left: 0 !important;
        padding-right: 0 !important
    }

    .ui-jqdialog .ui-widget-header .widget-header {
        border-bottom: none
    }

    .ui-jqdialog .ui-jqdialog-titlebar {
        border-bottom: 1px solid #DDD !important;
    }


    .ui-dialog .ui-dialog-buttonpane button,
    .ui-dialog .ui-jqdialog-buttonpane button,
    .ui-jqdialog .ui-dialog-buttonpane button,
    .ui-jqdialog .ui-jqdialog-buttonpane button {
        font-size: 14px !important;
    }

    .ui-jqdialog .ui-jqdialog-titlebar-close {
      position: absolute;
      top: 50%;
      width: 19px;
      margin: -12px 0 0;
      padding: 1px;
      height: 18px;
      cursor: pointer;
  }

    .ui-dialog .ui-dialog-titlebar-close,
    .ui-dialog .ui-jqdialog-titlebar-close,
    .ui-jqdialog .ui-dialog-titlebar-close,
    .ui-jqdialog .ui-jqdialog-titlebar-close {
        border: none !important;
        background: 0 0 !important;
        opacity: .4 !important;
        color: #D15B47 !important;
        padding: 0 !important;
        top: 50% !important;
        right: 8px !important;
        text-align: center !important;
    }

    .ui-dialog .ui-dialog-titlebar-close:before,
    .ui-dialog .ui-jqdialog-titlebar-close:before,
    .ui-jqdialog .ui-dialog-titlebar-close:before,
    .ui-jqdialog .ui-jqdialog-titlebar-close:before {
        content: "\f00d";
        display: inline;
        font-family: FontAwesome;
        font-size: 16px;
    }

    .ui-dialog .ui-dialog-titlebar-close:hover,
    .ui-dialog .ui-jqdialog-titlebar-close:hover,
    .ui-jqdialog .ui-dialog-titlebar-close:hover,
    .ui-jqdialog .ui-jqdialog-titlebar-close:hover {
        opacity: 1 !important;
        text-decoration: none !important;
        padding: 0 !important;
    }

    .ui-dialog .ui-dialog-titlebar-close .ui-button-text,
    .ui-dialog .ui-jqdialog-titlebar-close .ui-button-text,
    .ui-jqdialog .ui-dialog-titlebar-close .ui-button-text,
    .ui-jqdialog .ui-jqdialog-titlebar-close .ui-button-text {
        text-indent: 0 !important;
        visibility: hidden !important;
    }

    .ui-dialog .widget-header .ui-dialog-titlebar-close,
    .ui-dialog .widget-header .ui-jqdialog-titlebar-close,
    .ui-jqdialog .widget-header .ui-dialog-titlebar-close,
    .ui-jqdialog .widget-header .ui-jqdialog-titlebar-close {
        right: 10px !important;
    }

    .fm-button {
        margin: 0 4px !important;
        height: auto !important;
    }

    .fm-button:not(.btn) {
        background-color: #ABBAC3 !important;
        border-radius: 0 !important;
        box-shadow: none !important;
        color: #FFF !important;
        cursor: pointer !important;
        display: inline-block !important;
        font-size: 13px !important;
        line-height: 28px !important;
        padding: 0 12px 1px !important;
        margin: 0 8px !important;
        position: relative !important;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .25) !important;
        -webkit-transition: all .15s !important;
        -o-transition: all .15s !important;
        transition: all .15s !important;
        vertical-align: middle !important;
    }


    .ui-jqdialog .ui-jqdialog-content,
    .ui-jqdialog-content {
        font-size: 13px !important;
        padding: 4px 0 0 !important;
    }

    .ui-jqdialog .ui-jqdialog-content .formdata,
    .ui-jqdialog-content .formdata {
        font-size: 13px !important;
        padding: 6px 12px !important;
    }

    .ui-jqdialog .ui-jqdialog-content .form-view-data,
    .ui-jqdialog-content .form-view-data {
        vertical-align: middle !important;
        font-size: 13px !important;
    }

    .ui-jqdialog .ui-jqdialog-content[id*=alertcnt_],
    .ui-jqdialog-content[id*=alertcnt_] {
        padding: 8px 11px !important;
    }

    .ui-jqdialog-content .CaptionTD {
        font-size: 12px !important;
        text-align: right !important;
        color: #666 !important;
    }

    .ui-jqdialog-content .FormData {
        border-bottom: 1px dotted #E8E8E8 !important;
    }

    .fm-button.ui-state-default:hover {
        background-color: #8B9AA3 !important;
    }

    

    /*COLORES*/
    .dark {
        color: #333 !important;
    }

    .white {
        color: #FFF !important;
    }

    .red {
        color: #DD5A43 !important;
    }

    .red2 {
        color: #E08374 !important;
    }

    .light-red {
        color: #F77 !important;
    }

    .blue {
        color: #478FCA !important;
    }

    .light-blue {
        color: #93CBF9 !important;
    }

    .green {
        /*color: #69AA46 !important;*/
        color: #3d9970 !important;
    }

    .light-green {
        color: #B0D877 !important;
    }

    .orange {
        color: #FF892A !important;
    }

    .orange2 {
        color: #FEB902 !important;
    }

    .light-orange {
        color: #FCAC6F !important;
    }

    .purple {
        color: #A069C3 !important;
    }

    .pink {
        color: #C6699F !important;
    }

    .pink2 {
        color: #D6487E !important;
    }

    .brown {
        color: brown !important;
    }

    .grey {
        color: #777 !important;
    }

    /*PANEL*/
    .panel {
      margin-bottom: 0px !important;
      background-color: #fff !important;
      border: 1px solid transparent !important;
      border-radius: 4px !important;
      -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05) !important;
      box-shadow: 0 1px 1px rgba(0,0,0,.05) !important;
  }

  .panel .panel-heading {
    padding: 10px !important;
    background-color: #fff !important;
    border-top-right-radius: 3px !important;
    border-top-left-radius: 3px !important;
    border-bottom: 1px solid #dcdcdc !important;
    -webkit-border-radius: 0px !important;
    -moz-border-radius: 0px !important;
    border-radius: 0px !important;
}




  </style>
  <style>
 /*
 * Datatable Styles
 *
 */
  table.dataTable{
    font-size: 14px !important;
  }
  table.dataTable thead th{
    background-color:#3d9970 !important;
    color: #fff !important;
    border-bottom: 2px solid #BDBDBD !important;
  }

  table.dataTable thead th:hover{
    color: #fff  !important;
  }

  .box-body .dataTable tbody tr.even:hover, .dataTable tbody tr.even td.highlighted {
    background-color: #EFFBFB !important;
    color: #084B8A;    
  }
  

  .table-striped>tbody>tr:nth-of-type(odd) {
      background-color: #fff;
  }

  .table-striped>tbody>tr:nth-of-type(even) {
      background-color: #fff;
  }

  .box-body .dataTable tbody tr.odd:hover, .dataTable tbody tr.odd td.highlighted {
    background-color: #EFFBFB !important;
    color: #084B8A;
  }

table.dataTable thead th.sorting:after,
table.dataTable thead th.sorting_asc:after,
table.dataTable thead th.sorting_desc:after {
  position: absolute !important;
  top: 12px !important;
  right: 8px !important;
  display: block !important;
  font-family: FontAwesome !important;
}
table.dataTable thead th.sorting:after {
  content: "\f0dc" !important;
  color: #ddd !important;
  font-size: 0.8em !important;
  padding-top: 0.12em !important;
}
table.dataTable thead th.sorting_asc:after {
  content: "\f0de" !important;
  color: #000;
}
table.dataTable thead th.sorting_desc:after {
  content: "\f0dd" !important;
  color: #000;
}

  


/* ========================================================================
 * PAGINATION
 * ======================================================================== */
.pagination > li > a {
  color: #777;
}
.pagination > .active > a,
.pagination > .active > span {
  background-color: #3d9970 ;
  border: 1px solid #3d9970 ;
}
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  background-color: #1ab394;
  border: 1px solid #1ab394;
}
.pager > li > a {
  color: #777;
}





 /*
 * jqGRid Styles
 *
 */

  .ui-jqgrid .ui-jqgrid-view{
    font-size: 13px !important;
  }

  .ui-jqgrid .ui-jqgrid-view button,
  .ui-jqgrid .ui-jqgrid-view input,
  .ui-jqgrid .ui-jqgrid-view select,
  .ui-jqgrid .ui-jqgrid-view textarea {
    font-size: 13px !important;
  } 

  .ui-jqgrid .ui-jqgrid-htable thead {
      background-color: #EFF3F8 !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th span.ui-jqgrid-resize {
      height: 45px !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th div {
      padding-top: 12px;
      padding-bottom: 12px;
      overflow: visible;
  }

  .ui-jqgrid-hdiv .ui-jqgrid-htable {
      border-top: 1px solid transparent;
  }

  .ui-jqgrid-titlebar {
      position: relative;
      top: 1px;
      z-index: 1;
  }

  .ui-jqgrid tr.jqgrow,
  .ui-jqgrid tr.ui-row-ltr,
  .ui-jqgrid tr.ui-row-rtl {
      border: none !important;
  }

  .ui-jqgrid tr.ui-row-ltr td,
  .ui-jqgrid tr.ui-row-rtl td {
      border-bottom: 1px solid #E1E1E1 !important;
      padding: 6px 4px !important;
      border-color: #E1E1E1 !important;
  }

  .ui-jqgrid tr.ui-state-highlight.ui-row-ltr td {
      border-right-color: #C7D3A9
  }

  .ui-jqgrid tr.ui-state-highlight.ui-row-rtl td {
      border-left-color: #C7D3A9
  }

  .ui-jqgrid-btable .ui-widget-content.ui-priority-secondary {
      background-image: none;
      background-color: #FFF;
      opacity: 1;
  }

  .ui-jqgrid-btable .ui-widget-content.ui-state-hover {
      background-image: none;
      background-color: #A9F5D0;
      opacity: 1;
  }

  .ui-jqgrid-btable .ui-widget-content.ui-state-highlight {
      background-color: #E4EFC9
  }

  .ui-jqgrid .ui-jqgrid-pager {
      line-height: 15px;
      height: 55px;
      padding-top: 10px !important;
      padding-bottom: 10px !important;
      background-color: #EFF3F8 !important;
      border-bottom: 1px solid #E1E1E1 !important;
      border-top: 1px solid #E1E1E1 !important
  }

  .ui-jqgrid .ui-pg-input {
      font-size: inherit;
      width: 24px;
      height: 20px;
      line-height: 16px;
      -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box;
      text-align: center;
      padding-top: 1px;
      padding-bottom: 1px
  }



  .ui-jqgrid .ui-jqgrid-htable th div {     
      position:relative !important; 
      height: auto !important;     
      vertical-align: middle !important;
      white-space:normal !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th {     
     border-bottom: 1px #E1E1E1 dotted !important;
     vertical-align: top !important;
  }

  .ui-jqgrid .ui-jqgrid-labels th {
      border-right: 1px solid #E1E1E1!important;
      text-align: left!important;
  }
  .ui-jqgrid .ui-jqgrid-htable .ui-search-toolbar th {
    height: 30px !important;
    padding-top: 2px !important;
    white-space: normal !important;
  }

  .ui-jqgrid .ui-jqgrid-htable .ui-search-toolbar th div {
      padding-top: 0 !important;
      padding-bottom: 0 !important;
      height: 30px !important;
      line-height: 26px !important;
  }

  .ui-jqgrid .ui-jqgrid-htable thead {
      background-color: #EFF3F8 !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th span.ui-jqgrid-resize {
      height: 45px!important !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th div {
      padding-top: 12px !important;
      padding-bottom: 12px !important;
      overflow: visible !important;
  }

  .ui-jqgrid .ui-jqgrid-htable th.ui-th-column,.ui-th-column{  
      text-align: center !important;
      position:relative !important;
  }

  .ui-jqgrid .ui-pg-selbox {
      display: block;
      height: 24px !important;
      width: 60px;
      margin: 0;
      padding: 1px;
      line-height: normal;
      border: 1px #BDBDBD solid !important;
      font-size: 12px !important;
  }

  .ui-pg-table{
    font-size: 13px !important;
  }

  .ui-pg-input{
    border: 1px #BDBDBD solid !important;
    
  }
  .ui-pg-button{
    font-size: 15px;
    border: 0px solid #fff !important;    
  }

  .ui-pg-table>tbody>tr>.ui-pg-button>.ui-icon {
      display: inline-block;
      padding: 0;
      width: 24px;
      height: 24px;
      line-height: 22px;
      text-align: center;
      position: static;
      float: none;
      margin: 0 2px!important;
      color: grey;
      border: 1px solid #CCC;
      background-color: #FFF;
      border-radius: 100%;
  }
 

  .ui-paging-info{
    color: black;
    font-weight: bold !important;
  }
   

  .ui-jqgrid tr.jqgrow td {     
      border-bottom-color: transparent;
      border-right-color: transparent;
  }
  
  .ui-jqgrid .loading {
    position: absolute;
    top: 45%;
    left: 45%;
    width: auto;
    height: auto;
    z-index: 111;
    padding: 6px;
    margin: 5px;
    text-align: center;
    font-weight: 700;
    font-size: 12px;
    background-color: #FFF;
    border: 2px solid #8EB8D1;
    color: #E2B018;
  }

  .ui-jqgrid .frozen-bdiv, .ui-jqgrid .frozen-div {
      overflow: hidden;
      z-index: 9999;
      background-color: white;
  }  



  
  .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: none;
    background: #fff;
  }


  .ui-jqgrid .ui-jqgrid-htable .ui-th-div {
      height: auto !important;
      margin-top: 5px !important;
      display: inine-block !important;
      color: #408080 !important;
  }

  .ui-pg-div .fa {
        display: inline-block !important;
        width: 16px !important;
        float: none !important;
        position: static !important;
        text-align: center !important;
        opacity: .85 !important;
        -webkit-transition: all .12s !important;
        -o-transition: all .12s !important;
        transition: all .12s !important;
        margin: 0 1px !important;
        vertical-align: middle !important;
        cursor: pointer !important;
        font-size: 16px !important;
    }

  .ui-pg-div .ui-icon {
      display: inline-block;
      width: 18px;
      float: none;
      position: static;
      text-align: center;
      opacity: .85;
      -webkit-transition: all .12s;
      -o-transition: all .12s;
      transition: all .12s;
      margin: 0 1px;
      vertical-align: middle;
      cursor: pointer;
      font-size: 17px
  }

  .ui-pg-div .ui-icon:hover {
      -moz-transform: scale(1.2);
      -webkit-transform: scale(1.2);
      -o-transform: scale(1.2);
      -ms-transform: scale(1.2);
      transform: scale(1.2);
      opacity: 1;
      position: static;
      margin: 0 1px
  }

  .ui-pg-div .ui-icon:before {
      font-family: FontAwesome;
      display: inline !important;
  }


  .ui-jqgrid tr.jqgrow.ui-row-rtl td:last-child {
      border-right: none !important;
      border-left: 1px solid #E1E1E1
  }
  
  .ui-th-column{
    background-color: #F2F2F2 !important;
  }

  .ui-jqgrid .ui-jqgrid-hdiv {
      background-color: #EFF3F8 !important;
      border: 1px solid #D3D3D3 !important;
      border-width: 1px 0 0 1px !important;
      line-height: 15px !important;
      font-weight: 700 !important;
      color: #777 !important;
      text-shadow: none  !important;
  }

  th[aria-selected=true] .ui-jqgrid-sortable {
      color: #307ECC !important;
  }

  .ui-jqgrid-sortable {
      padding-left: 4px !important;
      font-size: 13px !important;
      color: #777 !important;
      font-weight: 700  !important;
  }

  .ui-jqgrid-sortable:hover {
      color: #547EA8  !important;
  }

  .ui-jqgrid .ui-jqgrid-btable {
    border-left: 1px solid #E1E1E1 !important;
  }

  .ui-pg-table .navtable .ui-corner-all {
        border-radius: 0 !important;
  }

  .ui-jqgrid .ui-pg-button .ui-separator {
      margin-left: 4px !important;
      margin-right: 4px !important;
      border-color: #C9D4DB !important;
  }

  .ui-jqgrid tr.ui-state-highlight.ui-row-ltr td {
      border-right-color: #C7D3A9;
  }
  
  
  .ui-jqgrid .ui-jqgrid-hbox {
      float: left !important;
      padding-right: 20px !important;
  }

/* ========================================================================
 * BOOTSTRAP OVERRIDE
 * ======================================================================= */
.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11,
.col-xs-12,
.col-sm-1,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-md-1,
.col-md-2,
.col-md-3,
.col-md-4,
.col-md-5,
.col-md-6,
.col-md-7,
.col-md-8,
.col-md-9,
.col-md-10,
.col-md-11,
.col-md-12,
.col-lg-1,
.col-lg-2,
.col-lg-3,
.col-lg-4,
.col-lg-5,
.col-lg-6,
.col-lg-7,
.col-lg-8,
.col-lg-9,
.col-lg-10,
.col-lg-11,
.col-lg-12 {
  padding-left: 10px;
  padding-right: 10px;
};
 

.mr-0{
  margin-right: 0 !important;
}
.mr-1 {
  margin-right: 0.25rem !important;
}
.ml-1{
  margin-left: 0.25rem !important;
}

.ml-2{
  margin-left: 0.5rem !important;
}
  
</style>

</head>
<body class="hold-transition skin-green-light sidebar-mini"><!-- sidebar-collapse -->
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>I</b>GO</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Inventory</b>GO</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <form class="navbar-form navbar-left hidden-xs" role="search">
          <div class="form-group">
            <input type="text" class="form-control" id="q" placeholder="Buscar..." style="width: 250px">
          </div>
      </form>

      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">        
          <!-- User Account: style can be found in dropdown.less -->          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>            
            <ul class="dropdown-menu" style="width: auto;">
            
              <li class="user-body">                
                <div class="pull-right">
                  
                 <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                                                   
                      Salir
                      &nbsp;
                      <i class="fa fa-fw fa-sign-out"></i>
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>

                </div>

              </li>

            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><b>MANTENIMIENTO</b></li>
        <li class="treeview
        {{ Request::is('products*','categories*','measures*','brands*','lines*') ? 'active menu-open' : '' }}
        ">
          <a href="#">
            <i class="fa fa-shield green"></i>
            <span>Inventario</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('products.index')
            <li class="{{ Request::is('products*') ? 'active' : '' }}"><a href="{{ url('/products') }}"><i class="fa fa-barcode"></i> <span>Productos</span></a></li>
            @endcan
            @can('categories.index')
            <li class="{{ Request::is('categories*') ? 'active' : '' }}"><a href="{{ url('/categories') }}"><i class="fa fa-barcode"></i> <span>Categorias</span></a></li>
            @endcan

            @can('measures.index')
            <li class="{{ Request::is('measures*') ? 'active' : '' }}"><a href="{{ url('/measures') }}"><i class="fa fa-barcode"></i> <span>Unidad de Medida</span></a></li>
            @endcan            

            @can('brands.index')
            <li class="{{ Request::is('brands*') ? 'active' : '' }}"><a href="{{ url('/brands') }}"><i class="fa fa-barcode"></i> <span>Marcas</span></a></li>
            @endcan

            @can('lines.index')
            <li class="{{ Request::is('lines*') ? 'active' : '' }}"><a href="{{ url('/lines') }}"><i class="fa fa-barcode"></i> <span>Lineas</span></a></li>
            @endcan
          </ul>
        </li>

        <li class="treeview 
        {{ Request::is('persons*') ? 'active menu-open' : '' }}
        ">
          <a href="#">
            <i class="fa fa-shield green"></i>
            <span>Personas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('persons.index')
            <li class="{{ Request::is('persons*') ? 'active' : '' }}"><a href="{{ url('/persons') }}"><i class="fa fa-barcode"></i> <span>Personas</span></a></li>
            @endcan           
          </ul>
        </li>

        <li class="treeview 
        {{ Request::is('invoices*') ? 'active menu-open' : '' }}
        ">
          <a href="#">
            <i class="fa fa-shield green"></i>
            <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('invoices.index')
            <li class="{{ Request::is('invoices*') ? 'active' : '' }}"><a href="{{ url('/invoices') }}"><i class="fa fa-barcode"></i> <span>Facturación</span></a></li>
            @endcan           
          </ul>
        </li>
       

        
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/users/create') }}"><i class="fa fa-circle-o"></i> Agregar</a></li>
            <li><a href="{{ url('/users') }}"><i class="fa fa-circle-o"></i> Listado</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-shield"></i>
            <span>Roles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/roles/create') }}"><i class="fa fa-circle-o"></i> Agregar</a></li>
            <li><a href="{{ url('/roles') }}"><i class="fa fa-circle-o"></i> Listado</a></li>
          </ul>
        </li>
     
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('content-header')
    

    <!-- Main content -->
    <section class="content">

      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/i18n/es.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('bower_components/moment/locale/es.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- JqGrid -->
<script src="{{ asset('bower_components/jqgrid/jqgrid.min.js') }}"></script>
<script src="{{ asset('bower_components/jqgrid/i18n/grid.locale-es.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- Bootbox -->
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<!-- Noty -->
<script src="{{ asset('bower_components/noty/noty.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('bower_components/jquery-validate/jquery.validate.min.js') }}"></script>

<!-- ap.js -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- ini.js -->
<script src="{{ asset('js/ini.js') }}"></script>
<!-- formula.js -->
<script src="{{ asset('js/formulas.js') }}"></script>
<!-- funciones.js -->
<script src="{{ asset('js/funciones.js') }}"></script>
<script>

  function updatePagerIcons(table) {
      var replacement = 
      {
          'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
          'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
          'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
          'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
      };
      $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
          var icon = $(this);
          var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
          
          if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
      });

      jQuery(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus light-blue");
      jQuery(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil blue");
      jQuery(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o light-red");
      jQuery(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search light-blue");
      jQuery(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh blue");
      jQuery(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-check green");
      jQuery(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times red");
      
      //jQuery(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
      //jQuery(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");
  } 

  $(document).ready(function () {
    $('.sidebar-menu').tree();

    /*=============================================
    Jquery Validate Message
    =============================================*/
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });

    $("body").on({
      mouseenter: function() {
        var texto=$(this).text();
        var css="position:absolute;right:-190px;background-color:black;color:white;z-index:100;";
        var css=css+"width:185px;padding:4px 4px;font-size:12px;";
      
        $("<div id='error_title' style='"+css+"'>"+texto+"</div>").insertBefore(this);
      },
      mouseleave: function() {
        $('#error_title').remove();
      }
    },'label.error');


    /*=============================================
    Data Table
    =============================================*/
    $.fn.select2.defaults.set('language', 'es');



    /*=============================================
    Data Table
    =============================================*/

    //Extension
    $.extend(true, $.fn.dataTable.defaults, {
        info: true,
        paging: true,
        ordering: true,
        searching: true,
        language: {
            //url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        lengthMenu: [
            [10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, 'Todos']
        ],
        dom: "Bfrtip",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print',
            'selected',
            'selectedSingle',
            'selectAll',
            'selectNone',
            'selectRows',
            'selectColumns',
            'selectCells'
        ],
        select: true
    });



    $(".datatable_1").DataTable({
        "searching": false,        

    });



    $(".datatable_2").DataTable({
        "searching": false,
        "ordering": false,
        "dom": "<'row'<'col-sm-6'l><'col-sm-6'p>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",        

    });

  })
</script>
@yield('scripts')
</body>
</html>
