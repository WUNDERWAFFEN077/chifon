<!-- form start -->
<div class="form-group">
  {{ Form::label('name', 'Nombre:', ['class' => 'col-sm-2 control-label required']) }}
  <div class="col-sm-8">
  {{ Form::text('name', null, ['class' => 'form-control input-sm col-sm-4', 'id' => 'name']) }}
  </div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-8">
		<a  type="button" class='btn btn-default' name='cancel' id='cancel' href="{{ route('categories.index') }}">Cancelar</a>
		{{ Form::submit('Guardar', ['class' => 'btn bg-olive']) }}
	</div>
</div>


