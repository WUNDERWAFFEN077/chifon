@extends('layouts.app')

@section('content-header')
<section class="content-header">
      <h1>
        Categorias
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Categorias</a></li>
        <li class="active">Listado</li>
      </ol>
    </section>
    </section>
@endsection

@section('content')      
<div class="box box-success">
    <!--<div class="panel-heading">
        Categorias
    </div>-->   
    <div class="box-header with-border">

    <a class="btn bg-olive margin" href="{{ route('categories.create') }}">                  
      <i class="fa fa-plus"></i>&nbsp;Nuevo
    </a>

    </div>
              

    <div class="box-body">
        <table id="dt_main" class="table table-bordered table-striped table-hover dt-head-center" width="100%">
            <thead>
                <tr>
                    <th width="10px" class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th width="80px" class="text-center">Acciones</th>
                </tr>
            </thead>
            <!--<tbody>
                @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>    
                    <td align="center">
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i></button>
                          <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            @can('categories.show')
                            <li><a href="{{ route('categories.show', $category->id) }}"><i class="fa fa-eye blue"></i>Ver</a></li>
                            @endcan

                            @can('categories.edit')
                            <li><a href="{{ route('categories.edit', $category->id) }}"><i class="fa fa-pencil green"></i>Editar</a></li>
                            @endcan

                            
                           @can('categories.destroy')
                            {!! Form::open(['route' => ['categories.destroy', $category->id], 
                            'method' => 'DELETE']) !!}
                            <li>                                
                            
                                <button class="btn btn-link btn_to_link" role="button">
                                    <i class="fa fa-trash-o red2"></i>Eliminar
                                </button>
                            {!! Form::close() !!}
                            </li>
                            @endcan                               
                               
                          </ul>
                        </div>
                    </td>                            
                    
                </tr>
                @endforeach
            </tbody>-->
        </table>
    
        <div class="row">
            <div class="col-md-12">
                <table id="grid-table"></table>

                <div id="grid-pager"></div>
                
            </div>
        </div>
        

        
    </div>
</div>
       
@endsection


@section('scripts')
<script type="text/javascript">  


    jQuery(document).ready(function ($) {
        $("#q").on("keypress", function(e){        
            var key = e.which;
            if (key == 13) {               
                dt_main.ajax.reload();                
                e.preventDefault();
            }
            
        });

        $(document).on("click", '.cmdEliminar', function(e) {
            e.preventDefault();
            var id = $(this).attr('param');            

            bootbox.confirm({ 
                size: "small",
                message: "Esta seguro de confirmar esta operación?", 
                buttons: {
                confirm: {
                    label: 'Aceptar',
                    className: 'bg-olive'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default'
                }
                },
                callback: function(result){ 
                    if (result) {
                        var url_delete = '{{ route("categories.destroy", ":id") }}';
                        url_delete = url_delete.replace(':id', id);
                        $.ajax({
                          type: "POST",
                          url: url_delete,
                          data: {_method: 'delete', _token: '{{csrf_token()}}'},
                          success: function (data) {                            
                            dt_main.ajax.reload();
                            if (data.status == 'success') {
                                
                                toastr.success(data.msg,'', {timeOut: 4000,positionClass: "toast-bottom-right",showDuration: 1000,progressBar: true,closeButton: true,extendedTimeOut: 1000});
                                
                                
                            }
                            
                          },
                          error: function (data) {
                            console.error('Error:', data);
                          }
                        });
                    }
                   
                }
            });

            

            
        });
       

         /*$('.formDelete').submit(function(e){
            if(!confirm('Do you want to delete this item?')){
                  e.preventDefault();
            }
          });*/
        

      var dt_main;

      dt_main =  $("#dt_main").DataTable({
            "searching": false,
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'p>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",  
            //scrollY: "30vh",  //Error autowidth
            autoWidth   : false,
            order: [[0, 'asc']],
            processing: true,
            serverSide: true,               
            "ajax":{
                url: "{{ url('categories/listGrid') }}",
                dataType : "json",
                type: "post",
                //data: {q: $("#qq").val(),_token: '{{csrf_token()}}'}
                data: function ( d ) {
                    d.q = $("#q").val(),
                    d._token = '{{csrf_token()}}';
                }
            },
             columns: [
                {name: "id", data: "id", sortable: true},
                {name: "name", data: "name", sortable: true},
                {name: "ayc", data: null, sortable: false}
            ], 
            "columnDefs": [ 
            { targets: 0, className: "text-center"},
            {
                "targets": -1,
                "className": "text-center",
                "data": "",
                "render" : function ( data, type, row, meta ) {      
                    //console.log(meta);return false;
                    var id = data.id;
                    var url_show = '{{ route("categories.show", ":id") }}';
                    url_show = url_show.replace(':id', id);
                    var url_edit = '{{ route("categories.edit", ":id") }}';
                    url_edit = url_edit.replace(':id', id);
                    var url_delete = '{{ Form::open(['route' => ['categories.destroy', ':id'], 'class' => 'formDelete', 
                            'method' => 'DELETE']) }}';
                    url_delete = url_delete.replace(':id', id);
                    url_delete_end = '{{ Form::close() }}';
                    return '<div class="btn-group">'+
                          '<button type="button" class="btn bg-olive btn-xs"><i class="fa fa-cog"></i></button>'+
                          '<button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '<span class="caret"></span>'+
                            '<span class="sr-only">Toggle Dropdown</span>'+
                          '</button>'+
                          '<ul class="dropdown-menu dropdown-menu-right">'+
                            @can('categories.show')'<li><a href="'+url_show+'"><i class="fa fa-eye blue"></i>Ver</a></li>'@endcan+                          
                            @can('categories.edit')'<li><a href="'+url_edit+'"><i class="fa fa-pencil green"></i>Editar</a></li>'@endcan+   
                            @can('categories.destroy')
                            url_delete+'<li>'+
                                '<button class="btn btn-link btn_to_link cmdEliminar" role="button" param="'+id+'" >'+
                                    '<i class="fa fa-trash-o red2"></i>Eliminar'+
                                '</button>'+                            
                               
                            url_delete_end+'</li>'    
                                                    
                            @endcan     
                            +            
                          '</ul>'+
                        '</div>'+
                    '</td>';
                }

            } ],
            drawCallback: function (settings) {
               //console.log(settings);
            },           
            initComplete: function( settings, json ) {
               //console.log(json);
                
            }
        });

    });

    jQuery(function($) {
        var grid_selector = "#grid-tablex";
        var pager_selector = "#grid-pagerx";
        
        var parent_column = $(grid_selector).closest('[class*="col-"]');
       
        //resize to fit page size
       /* $(window).on('resize.jqGrid', function () {
            $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
          
        });
        

        $(document).on('collapsed.pushMenu expanded.pushMenu', function(ev){
            //PUT YOUR CODE HERE 
            
            var event_name = ev.type;
            if( event_name === 'collapsed' || event_name === 'expanded' ) {
                //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
                setTimeout(function() {                                        
                    $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
                }, 500);
            }
        });*/        


        getColumnIndexByName = function (grid, columnName) {
            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l = cm.length;
            for (i = 0; i < l; i++) {
                if (cm[i].name === columnName) {
                    return i; // return the index
                }
            }
            return -1;
        };
        
    
    
        jQuery(grid_selector).jqGrid({           
            url: "{{ url('categories/listGrid') }}",
            editurl: "{{ url('categories/1/edit') }}",
            //data: grid_data,
            datatype: "json",
            postData: {_token: '{{csrf_token()}}' },
            mtype:"POST",
            height: 250,
            colNames:['ID','Nombre', 'Acciones'],
            colModel:[
                {name:'id',index:'id', align:'center',sortable:false, width:5, search : false},
                {name:'name',index:'name', align:'center',sortable:false, search : false, editable: true},
                {name:'myac',index:'', align:'center', width:100, fixed:true, sortable:false, resize:false,
                formatter:'actions', 
                formatoptions:{ 
                    keys:true,
                    //editbutton: true, 
                    delbutton: true,                  
                    delOptions: {
                        url: "{{ url('categories/listGrid') }}",
                        caption: 'Eliminar',
                        width: 'auto',
                        msg: 'Desea eliminar el producto?',
                        bSubmit: "Aceptar",
                        bCancel: "Cancelar",
                        mtype: 'POST',                        
                        beforeShowForm: function($form) {                            
                             var dlgDiv = $("#delmodgrid-table");
                             //console.log(dlgDiv);
                             //var parentDiv = dlgDiv.parent();
                             var dlgWidth = dlgDiv.width();
                             //var parentWidth = parentDiv.width();
                             var dlgHeight = dlgDiv.height();
                             //var parentHeight = parentDiv.height();
                             // TODO: change parentWidth and parentHeight in case of the grid
                             var parentHeight = $(window).height();
                             var parentWidth = $(window).width();
                             //       is larger as the browser window
                             //dlgDiv[0].style.top = Math.round((parentHeight-dlgHeight)/2) + "px";
                             dlgDiv[0].style.left = Math.round((parentWidth-dlgWidth)/2) + "px";
                             
                        },                        
                        afterComplete: function (response, postdata) {
                            var message_result = 'El producto ha sido eliminado.';
                            $("#message").removeClass('hidden');
                            $("#user-message").html(message_result);
                            $("#message").fadeTo(2000, 500).slideUp(500, function(){
                                $("#message").slideUp(500);
                                $("#user-message").html('');
                                //$("#message").alert('close');
                            }); 


                           
                        },
                    }
                }
            },
            ], 
    
            viewrecords : true,
            rowNum:10,
            rowList:[10,20,30],
            pager : pager_selector,
            altRows: true, 
            loadComplete : function(data) {     
                  //console.log(data);
                  var table = this;

                  setTimeout(function(){                    
                      updatePagerIcons(table);
                  }, 0);

                  
                    var count=jQuery(grid_selector).jqGrid('getGridParam', 'records');

                    var iCol = getColumnIndexByName(jQuery(grid_selector), 'myac');

                    $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")")
                    .each(function() {
                        $("<div>", {
                            title: "Procesar documento",
                                mouseover: function() {
                                    $(this).addClass('ui-state-hover');
                                },
                                mouseout: function() {
                                    $(this).removeClass('ui-state-hover');
                                },
                                click: function(e) {                                
                                    id = $(e.target).closest("tr.jqgrow").attr("id");
                                   
                                    //confirmProcesar(id);
                                }
                            }
                        ).css({"margin-left": "8px","margin-top": "0px", float: "left", cursor: "pointer"})
                        .addClass("ui-pg-div ui-inline-custom")
                        .append('<span class="ui-icon fa fa-cog grey"></span>')
                        //.prependTo($(this).children("div"));
                        .appendTo($(this).children("div"));
                        
                    });

                  
            },     
            loadError: function (jqXHR, textStatus, errorThrown) {
                console.log('HTTP status code: ' + jqXHR.status + 'n' +
                'textStatus: ' + textStatus + 'n' +
                'errorThrown: ' + errorThrown);
                console.log('HTTP message body (jqXHR.responseText): ' + 'n' + jqXHR.responseText);
            },
            sortname: "id",
            sortorder: "asc",     
            autowidth: true,
            gridview: true,//Para acelerar carga, pero no podemos utilizar treeGrid, subGrido el evento afterInsertRow
            
    
    
        });
        $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

        

    });


    
</script>   

@endsection