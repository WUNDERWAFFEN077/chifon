@extends('layouts.app')
@section('content-header')
<section class="content-header">
      <h1>
        Categorias
      </h1>
      <ol class="breadcrumb">
        <li>
            <a href="{{ route('categories.index') }}">
            <i class="glyphicon glyphicon-list-alt"></i>Lista de categorias</a><i></i>
        </li>
      </ol>
    </section>
    </section>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Informaci&oacute;n B&aacute;sica</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">                    
                    {{ Form::open(['route' => 'categories.store', 'class'=>'form-horizontal']) }}

                        @include('categories.partials.form')
                        
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection