@extends('layouts.app')

@section('content-header')
<section class="content-header">
      <h1>
        {{ $titulo }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">{{ $titulo }}</a></li>
        <li class="active">Listado</li>
      </ol>
    </section>
    </section>
@endsection

@section('content')      
<div class="box box-success">
    <!--<div class="panel-heading">
        Categorias
    </div>-->   
    <div class="box-header with-border">

    <a class="btn bg-olive margin" href="{{ route('invoices.create') }}">                  
      <i class="fa fa-plus"></i>&nbsp;Nuevo
    </a>

    </div>
              

    <div class="box-body">
        <table id="dt_main" class="table table-bordered table-striped table-hover dt-head-center" width="100%">
            <thead>
                <tr>
                    <th width="10px" class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Código</th>
                    <th width="50px" class="text-center">U.M</th>
                    <th class="text-center">Linea</th>
                    <th class="text-center">Categoria</th>
                    <th width="80px" class="text-center">Acciones</th>
                </tr>
            </thead>            
        </table>
    
        <div class="row">
            <div class="col-md-12">
                <table id="grid-table"></table>

                <div id="grid-pager"></div>
                
            </div>
        </div>
        

        
    </div>
</div>
       
@endsection


@section('scripts')
<script type="text/javascript">  


    jQuery(document).ready(function ($) {
        $("#q").on("keypress", function(e){        
            var key = e.which;
            if (key == 13) {               
                dt_main.ajax.reload();                
                e.preventDefault();
            }
            
        });
       

      var dt_main;

      dt_main =  $("#dt_main").DataTable({
            "searching": false,
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'p>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",  
            //scrollY: "30vh",  //Error autowidth
            autoWidth   : false,
            order: [[0, 'asc']],
            processing: true,
            serverSide: true,               
            "ajax":{
                url: "{{ url('invoices/listGrid') }}",
                dataType : "json",
                type: "post",
                //data: {q: $("#qq").val(),_token: '{{csrf_token()}}'}
                data: function ( d ) {
                    d.q = $("#q").val(),
                    d._token = '{{csrf_token()}}';
                }
            },
             columns: [
                {name: "id", data: "id", sortable: true},
                {name: "name", data: "name", sortable: true},
                {name: "reference_code", data: "reference_code", sortable: false},
                {name: "measure", data: "measure", sortable: false},
                {name: "line", data: "line", sortable: false},
                {name: "category", data: "category", sortable: false},
                {name: "ayc", data: null, sortable: false}
            ], 
            "columnDefs": [ 
            { targets: [0,2,3], className: "text-center"},
            {
                "targets": -1,
                "className": "text-center",
                "data": "",
                "render" : function ( data, type, row, meta ) {      
                    //console.log(meta);return false;
                    var id = data.id;
                    var url_edit = '{{ route("invoices.edit", ":id") }}';
                    url_edit = url_edit.replace(':id', id);                  
                    return '<div class="btn-group">'+
                          '<button type="button" class="btn bg-olive btn-xs"><i class="fa fa-cog"></i></button>'+
                          '<button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                            '<span class="caret"></span>'+
                            '<span class="sr-only">Toggle Dropdown</span>'+
                          '</button>'+
                          '<ul class="dropdown-menu dropdown-menu-right">'+ 
                            @can('invoices.edit')'<li><a href="'+url_edit+'"><i class="fa fa-pencil green"></i>Editar</a></li>'@endcan+
                          '</ul>'+
                        '</div>'+
                    '</td>';
                }

            } ],
            drawCallback: function (settings) {
               //console.log(settings);
            },           
            initComplete: function( settings, json ) {
               //console.log(json);
                
            }
        });

    });

    
</script>   

@endsection