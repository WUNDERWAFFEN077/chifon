<!-- form start -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>                    
    </div>
@endif

<div class="invoice">
  <div class="form-group">
  {{ Form::label('people_id', 'Cliente:', ['class' => 'col-sm-2 control-label-xs required select2']) }}
  <div class="col-sm-4">
  <!--{{ Form::text('people_id', null, ['class' => 'form-control form-control-xs', 'id' => 'people_id']) }}-->
  <select id="people_id" name="people_id" class="form-control form-control-xs required">
  </select>
  </div>

  {{ Form::label('issue_date', 'Fecha de emisión:', ['class' => 'col-sm-2 control-label-xs required']) }}
  <div class="col-sm-4">
      <div class="input-group date" style="width: 140px;">
      <div class="input-group-addon input-group-addon-xs">
        <i class="fa fa-calendar"></i>
      </div>
      {{ Form::text('issue_date', null, ['class' => 'form-control form-control-xs pull-right date', 'id' => 'issue_date']) }} 
      <span class="tooltip--right" data-tooltip="Fecha en la que se emite la venta."><i class="fa fa-question-circle green ml-2"></i></span>
      
    </div>
  </div>
  
  
</div>

<div class="form-group">
{{ Form::label('issue_date', 'Fecha de emisión:', ['class' => 'col-sm-2 control-label-xs required']) }}
  <div class="col-sm-4">
      <div class="input-group date" style="width: 140px;">
      <div class="input-group-addon input-group-addon-xs">
        <i class="fa fa-calendar"></i>
      </div>
      {{ Form::text('issue_date', null, ['class' => 'form-control form-control-xs pull-right date', 'id' => 'issue_date']) }} 
      <span class="tooltip--right" data-tooltip="Fecha en la que se emite la venta."><i class="fa fa-question-circle green ml-2"></i></span>
      
    </div>
  </div>

  {{ Form::label('due_date', 'Fecha de vencimiento:', ['class' => 'col-sm-2 control-label-xs required']) }}
  <div class="col-sm-4">
      <div class="input-group date" style="width: 140px;">
        <div class="input-group-addon input-group-addon-xs">
          <i class="fa fa-calendar"></i>
        </div>
        {{ Form::text('due_date', null, ['class' => 'form-control form-control-xs pull-right date', 'id' => 'due_date']) }}
        <span class="tooltip--right" data-tooltip="Fecha de vencimiento. Calculada segun el tipo de pago."><i class="fa fa-question-circle green ml-2"></i></span>
      </div>  
  </div>
</div>


<!-- Detalle -->
<!--<div class="table-responsive">
    <table class="table invoice-items no-border" cellspacing="0" cellpadding="0">
        <thead>
              <tr>      
                <th class="text-center">Producto</th>
                <th width="120px" class="text-center">Cantidad</th>
                <th width="120px" class="text-center">Precio</th>        
                <th width="120px" class="text-center">Sub Total</th>
                <th width="100px">&nbsp;</th>
              </tr>
        </thead>
        <tbody id="content-items">        
          <tr class="rowid" param="1">
            <input type="hidden" id="row_id1" value="1">            
            <td>                    
              <div style="position: relative;">
              <select name="product_id1" class="form-control form-control-xs product" id="product_id1" ></select>
              </div>
            </td>

            <td>
              <div style="position: relative;">
              <input name="quantity1" id="quantity1" class="form-control form-control-xs only_select quantity text-right" type="text" placeholder="Cantidad" value="">
              </div>
            </td>

            <td>
              <div style="position: relative;">
              <input name="price_amount1" id="price_amount1" class="form-control form-control-xs only_select price text-right" type="text" placeholder="Precio" value="">
              </div>
            </td>

            <td>
              <input name="total_amount1" id="total_amount1" type="text" class="form-control form-control-xs text-right" readonly value="">
            </td>

            <td class='text-center'> 
            <i onclick='removeItem(this)' style='cursor:pointer' aria-hidden='true' class='fa fa-lg fa-remove red'></i> 
            </td>
          </tr>   
          <input type="hidden" id="counter" value="1">         
        </tbody>
    </table>
    <button type="button" class="mb-xs mt-xs mr-xs btn btn-success" onclick="addItem()">
        <i class="fa fa-plus"></i>
    </button>
    <table class="table invoice-footer no-border" cellspacing="0" cellpadding="0">
      <tr>
          <td>&nbsp;</td>
          <td width="220px">Sub Total</td> 
          <td width="120px" class="text-right">100.00</td>
          <td width="100px">&nbsp;</td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td width="220px">IGV</td> 
          <td width="120px" class="text-right">18.00</td>
          <td width="100px">&nbsp;</td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td width="220px">Total</td> 
          <td width="120px" class="text-right">118.00</td>
          <td width="100px">&nbsp;</td>
      </tr>
    </table>  
</div>

</div>-->





<div class="form-group">
  <div class="col-sm-12 text-center">
    <a  type="button" class='btn btn-default' name='cancel' id='cancel' href="{{ route('persons.index') }}">Cancelar</a>
    {{ Form::submit('Generar Factura', ['class' => 'btn bg-olive','id'=>'cmdGenerar']) }}
  </div>
</div>


@section('scripts')
<script type="text/javascript">
  
  $(function(){
    
    //$.validator.setDefaults({ ignore: [".ignore"] });
    __productAutocomplete();

  

    

    $("select").on("select2:close", function (e) {  
        //$(this).valid(); 
    });

    $('#radioBtn a').on('click', function(){
      var sel = $(this).data('title');
      var tog = $(this).data('toggle');
      $('#'+tog).prop('value', sel);
      
      $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
      $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });

    var n_dec = 2;
    $("body").on("keyup", "[id^=quantity], [id^=price_amount]", function(e) {
        var row = $(this).closest('.rowid');
        ix = row.attr("param");
        console.log(ix);
        
        var quantity = $("#quantity"+ix).val();
        var price_amount = $("#price_amount"+ix).val();
        total_amount = parseFloat(quantity) * parseFloat(price_amount);
        total_amount = precise_round(total_amount,n_dec);

        $("#total_amount"+ix).val(total_amount);
        
        calcularTotal();
    });

    //SELECT people
    $('#people_id').select2({
        placeholder: "Buscar cliente...",
        minimumInputLength: 1,
        ajax: {
            url: '{{ url("invoices/selectpeople") }}',
            dataType: 'json',
            data: function (params) {
              //console.log(params);
                return {
                    q: $.trim(params.term),
                    //_token : '{{csrf_token()}}'
                };
            },
            processResults: function (data) {
              //console.log(data);
                return {
                    results: data
                };
            },
            //cache: true
        }
    });


    
    
    

    moment.locale('es');
    //Ussue Date
    $("#issue_date").val(moment().format('DD/MM/YYYY'));
    $("#due_date").val(moment().format('DD/MM/YYYY'));
    /*=============================================
    RANGO DE FECHAS
    =============================================*/

    
   

    $('#issue_date').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true,     
    }).on('change', function() {
        //$(this).valid();
    });

    $('#due_date').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true
    }).on('change', function() {
        //$(this).valid();  // triggers the validation test
    });
    
  

    //Probar Ajax envio
    $("#cmdGenerar").on("click",function(e){
      generarComprobante();
    });

  });

  function toDate(value){
    //String:dd/mm/yyyy a Fecha
    var from = value.split("/")
    var f = new Date(from[2], from[1] - 1, from[0]);   
    return f;
  }

  function dateDMY(value){
    //String: dd/mm/yyyy a yyyy-mm-dd
    var from = value.split("/")
    var f = new Date(from[2], from[1] - 1, from[0]); 
    return moment(f).format("YYYY-MM-DD");
  }


  $.validator.addMethod("dateFormat",
    function(value, element) {
      //return value.match(/^dd?-dd?-dd$/);
      var f = toDate(value);
      //console.log(f);
      return moment(f, 'DD/MM/YYYY',true).isValid();
      
    },
    "Por favor ingrese una fecha en formato dd/mm/yyyy.");  



  

  function generarComprobante(){    
    /*
    $.validator.addClassRules("product", {
      required: true
    });
    $.validator.addClassRules("quantity", {
      required: true
    });
    $.validator.addClassRules("price", {
      required: true
    });
    
    $('#form-invoice').validate({
      //ignore: ".ignore",
      //onkeyup: false,
      rules: {        
        people_id:"required",        
        issue_date: {     
            date: false, //Desactivar causa errores
            dateFormat: true,    
            
        },
        due_date: {
          date: false, //Desactivar causa errores
          dateFormat: true,
          
        },

      
      },      
      success: function(element) {
        element
        .text('OK!').addClass('valid')
        .closest('.control-group').removeClass('error').addClass('success');
      },
      submitHandler: function(form) {
        //alert("VALIDAO");
      }

      
    });


    //REVISAR VALIDACION DE FORMULARIO
    var valid = $('#form-invoice').valid();

    if (!valid) {
       new Noty({
          theme: 'bootstrap-v3',
          killer: true,
          layout: 'topCenter',
          closeWith: ['click', 'button'],
          type: 'error',       
          timeout: 3500,
          text: 'Corregir los errores marcados en rojo', 
          animation: {
              open: 'animated fadeInDown',
              close: 'animated fadeOutUp', 
              easing: 'swing', 
              speed: 1000
          }
      }).show(); 
       return false;
    }

    */

   

    var invoice_type = $("#invoice_type").val();
    var people_id = $("#people_id").val();

    var issue_date=  $("#issue_date").val();
    var due_date=  $("#due_date").val();

    issue_date = dateDMY(issue_date);
    due_date = dateDMY(due_date);

    
    var tableLength = $("#content-items tr").length;
    //console.log(issue_date);
    var detalle = [];
    $("body").find("input[id^=row_id]").each(function(){
        var ix = $(this).val();     
        var product = {};        
        var product_id = $("#product_id"+ix).val();
        var quantity = $("#quantity"+ix).val();quantity = parseFloat(quantity)||0;
        var price_amount = $("#price_amount"+ix).val();price_amount = parseFloat(price_amount)||0;
        var total_amount = $("#total_amount"+ix).val();total_amount = parseFloat(total_amount)||0;

        if(product_id!='' && quantity>0 && price_amount>0 && total_amount>0){
          //producto['id'] = orden;
          product['product_id'] = product_id;
          product['quantity'] = quantity;
          product['price_amount'] = price_amount;
          product['total_amount'] = total_amount;
          detalle.push(product);
        }        
        //console.log(quantity);
    });

    //REVISAR DETALLE
    if (detalle.length<=0) {

       new Noty({
          theme: 'bootstrap-v3',
          killer: true,
          layout: 'topCenter',
          closeWith: ['click', 'button'],
          type: 'error',       
          text: 'Debe agregar al menos un producto.', 
          animation: {
              open: 'animated fadeInDown',
              close: 'animated fadeOutUp', 
              easing: 'swing', 
              speed: 1000
          }
      }).show(); 
      
      return false;
    
    }
    return false;
    
    $.ajax({
        type: "POST", 
        url: '{{ url("invoices/store") }}', 
        data : {  
          _token: '{{csrf_token()}}',
          invoice_type: invoice_type,
          people_id: people_id,
          issue_date:issue_date,
          due_date:due_date,
          data: detalle     
          },
        /*dataType: "json",*/
        /*async: false,*/
        success: function(response){
          console.log(response);
          //window.location.href = '{{ url("invoices") }}';           
            
        },
        error: function(){
          //alert(r.message);
        }
      });

  }

  
  function addItem(){
      var ncount = $("#counter").val();
      ncount++;
      $("#content-items").append(
            '<tr class="rowid" param="'+ncount+'">'+          
            '<input type="hidden" id="row_id'+ncount+'" value="'+ncount+'">'+
            '<td>'+
              '<div style="position: relative;">'+
              '<select name="product_id'+ncount+'" class="form-control form-control-xs product" id="product_id'+ncount+'"></select>'+
              '</div>'+
            '</td>'+
            '<td>'+
              '<div style="position: relative;">'+
              '<input name="quantity'+ncount+'" id="quantity'+ncount+'" class="form-control form-control-xs only_select quantity text-right" type="text" placeholder="Cantidad" value="">'+
              '</div>'+
            '</td>'+
            '<td>'+
              '<div style="position: relative;">'+
              '<input name="price_amount'+ncount+'" id="price_amount'+ncount+'" class="form-control form-control-xs only_select price text-right" type="text" placeholder="Precio" value="">'+
              '</div>'+
            '</td>'+
            '<td>'+
              '<input  id="total_amount'+ncount+'" class="form-control form-control-xs text-right" type="text" readonly value="">'+
            '</td>'+
            '<td class="text-center">'+
              "<i onclick='removeItem(this)' style='cursor:pointer' aria-hidden='true' class='fa fa-lg fa-remove red'></i>"+
            '</td>'+
            "</tr>"
      );
      
      $("#counter").val(ncount);
      __productAutocomplete();
  }

  function removeItem(el){
        //Remove of JSON
        //var codproducto = $(el).parent().parent().find('td:eq(5)').children().val()        
        //var i=itemsFactura.length;
        /*while (i--) {
            if(codproducto == itemsFactura[i].codigo_del_producto){
                itemsFactura.splice(i,1)
            }
        }*/
        //Remove of DOM
        $(el).parent().parent().remove()

    }

    function calcularTotal(){
      var total = 0;
      var invoice_total = product_total = 0;
      $("body").find("input[id^=row_id]").each(function(){
          var ix = $(this).val();
          
          var quantity = $("#quantity"+ix).val();
          var producto_igv = 18;
          var price_amount = $("#price_amount"+ix).val();

          total_amount = quantity * price_amount;
          $("#total_amount"+ix).val(total_amount);

          invoice_total += quantity * price_amount * (1 + (producto_igv/100.00));
          
      });

      //TOTALES
      igv = Formulas.calcularIgv(invoice_total);
      subTotal = Formulas.calcularMontoSinIgv(invoice_total);

      igv = parseFloat(igv).format(2);
      subTotal = parseFloat(subTotal).format(2);
      total = parseFloat(invoice_total).format(2);

      console.log(igv);
      console.log(subTotal);
      console.log(total);

     //$("#sub_total").html(subTotal);
     //$("#igv").html(igv);
     //$("#total").html(total);

    }

    function __productAutocomplete(){
      $("select[id^=product_id]").select2({
        placeholder: "Buscar producto...",
        minimumInputLength: 1,
        ajax: {
            url: '{{ url("invoices/selectproduct") }}',
            dataType: 'json',
            data: function (params) {
              //console.log(params);
                return {
                    q: $.trim(params.term),
                    //_token : '{{csrf_token()}}'
                };
            },
            processResults: function (data) {
              //console.log(data);
                return {
                    results: data
                };
            },
            //cache: true
        }
      });
      
    }
</script>
@endsection





