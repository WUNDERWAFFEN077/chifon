@extends('layouts.app')
@section('content-header')
<section class="content-header">
      <h1>
        {{ $titulo2 }}
      </h1>
      <ol class="breadcrumb">
        <li>
            <a href="{{ route('invoices.index') }}">
            <i class="glyphicon glyphicon-list-alt"></i>Lista de {{ $titulo }}</a><i></i>
        </li>
      </ol>
    </section>
    </section>
@endsection

@section('content')
<div class="container-left">
    <div class="row">
        <div class="col-md-12">
    
            {{ Form::open(['route' => 'invoices.store', 'class'=>'form-horizontal', 'id'=>'form-invoice']) }}

                @include('invoices.partials.form')
                
            {{ Form::close() }}
               
        </div>
    </div>
</div>
@endsection