var Formulas = (function(){
    var igv = 0.18;

    return {
        calcularIgv: function(monto) {
            var subTotal = Formulas.calcularMontoSinIgv(monto);
            return monto - subTotal;
        },
        calcularMontoSinIgv: function(monto){
            return monto / (1 + igv);
        },
        calcularUtilidad: function(costo, ingreso) {
            return ingreso - costo;
        },
        calcularMargenUtilidad: function(costo, ingreso) {
            return ((ingreso - costo) / costo)*100;
        },
        calcularTasaEfectivaAnual: function(capital, interes, periodos) {
            return capital * Math.pow(1 + interes, periodos);
        }
    };
})();