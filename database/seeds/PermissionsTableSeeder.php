<?php

use Illuminate\Database\Seeder;

use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Users
        Permission::create([
            'name'          => 'Navegar usuarios',
            'slug'          => 'users.index',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de usuario',
            'slug'          => 'users.show',
            'description'   => 'Ve en detalle cada usuario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Edición de usuarios',
            'slug'          => 'users.edit',
            'description'   => 'Podría editar cualquier dato de un usuario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar usuario',
            'slug'          => 'users.destroy',
            'description'   => 'Podría eliminar cualquier usuario del sistema',      
        ]);

        //Roles
        Permission::create([
            'name'          => 'Navegar roles',
            'slug'          => 'roles.index',
            'description'   => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un rol',
            'slug'          => 'roles.show',
            'description'   => 'Ve en detalle cada rol del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de roles',
            'slug'          => 'roles.create',
            'description'   => 'Podría crear nuevos roles en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de roles',
            'slug'          => 'roles.edit',
            'description'   => 'Podría editar cualquier dato de un rol del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar roles',
            'slug'          => 'roles.destroy',
            'description'   => 'Podría eliminar cualquier rol del sistema',      
        ]);


        //Categories
        Permission::create([
            'name'          => 'Navegar categorias',
            'slug'          => 'categories.index',
            'description'   => 'Lista y navega todos los categorias del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un categoria',
            'slug'          => 'categories.show',
            'description'   => 'Ve en detalle cada categoria del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de categorias',
            'slug'          => 'categories.create',
            'description'   => 'Podría crear nuevos categorias en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de categorias',
            'slug'          => 'categories.edit',
            'description'   => 'Podría editar cualquier dato de un categoria del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar categorias',
            'slug'          => 'categories.destroy',
            'description'   => 'Podría eliminar cualquier categoria del sistema',      
        ]);

        //Measures
        Permission::create([
            'name'          => 'Navegar U.M',
            'slug'          => 'measures.index',
            'description'   => 'Lista y navega todas las U.M. del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de una U.M.',
            'slug'          => 'measures.show',
            'description'   => 'Ve en detalle cada U.M. del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de U.M',
            'slug'          => 'measures.create',
            'description'   => 'Podría crear nuevos U.M. en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de U.M',
            'slug'          => 'measures.edit',
            'description'   => 'Podría editar cualquier dato de una U.M. del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar U.M',
            'slug'          => 'measures.destroy',
            'description'   => 'Podría eliminar cualquier U.M. del sistema',      
        ]);
        

        //Products
        Permission::create([
            'name'          => 'Navegar productos',
            'slug'          => 'products.index',
            'description'   => 'Lista y navega todos los productos del sistema',
        ]);

        Permission::create([
            'name'          => 'Ver detalle de un producto',
            'slug'          => 'products.show',
            'description'   => 'Ve en detalle cada producto del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Creación de productos',
            'slug'          => 'products.create',
            'description'   => 'Podría crear nuevos productos en el sistema',
        ]);
        
        Permission::create([
            'name'          => 'Edición de productos',
            'slug'          => 'products.edit',
            'description'   => 'Podría editar cualquier dato de un producto del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Eliminar productos',
            'slug'          => 'products.destroy',
            'description'   => 'Podría eliminar cualquier producto del sistema',      
        ]);


    }
}
