<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class, 20)->create();
        User::create([
            'name' => 'Aj',
            'email' => 'arudas@acuario.com.pe',
            'password' => bcrypt('123456'),
            'remember_token' => str_random(10),                  
        ]
        );

        Role::create([
        	'name'		=> 'Admin',
        	'slug'  	=> 'admin',
        	'special' 	=> 'all-access'
        ]);

        Role::create([
            'name'      => 'Sin acceso',
            'slug'      => 'denegado',
            'special'   => 'no-access'
        ]);
    }
}
