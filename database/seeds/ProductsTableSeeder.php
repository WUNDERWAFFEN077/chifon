<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\Product::class, 80)->create();

        //Category
        App\ProductCategory::create([
            'name' => 'Sin especificar',                 
        ]
        );

        

        //Brand
        App\ProductBrand::create([
            'name' => 'Sin especificar',                 
        ]
        );

        //Line
        App\ProductLine::create([
            'name' => 'Sin especificar',           
            'brand_id' => 1,
        ]
        );

        //Measure
        App\ProductMeasure::create([
            'code' => '01',           
            'reference_code' => 'KG',    
            'name' => 'Kilogramos',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '02',        
            'reference_code' => 'LB',     
            'name' => 'Libras',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '03',           
            'reference_code' => 'TNL',     
            'name' => 'Toneladas largas',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '04',         
            'reference_code' => 'TNM',   
            'name' => 'Toneladas métricas',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '05',         
            'reference_code' => 'TNC',   
            'name' => 'Toneladas cortas',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '06',           
            'reference_code' => 'GR', 
            'name' => 'Gramos',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '07',           
            'reference_code' => 'UND', 
            'name' => 'Unidades',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '08',         
            'reference_code' => 'LT',   
            'name' => 'Litros',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '09',     
            'reference_code' => 'GL',       
            'name' => 'Galones',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '10',     
            'reference_code' => 'BAR',       
            'name' => 'Barriles',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '11',     
            'reference_code' => 'LAT',       
            'name' => 'Latas',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '12',       
            'reference_code' => 'CJA',     
            'name' => 'Cajas',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '13',           
            'reference_code' => 'MIL', 
            'name' => 'Millares',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '14',  
            'reference_code' => 'M3',          
            'name' => 'Metros cúbicos',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '15',  
            'reference_code' => 'MT',          
            'name' => 'Metros',
        ]        
        );
        App\ProductMeasure::create([
            'code' => '99', 
            'reference_code' => 'NE',           
            'name' => 'Sin especificar',
        ]        
        );

        //Existence
        App\ProductExistence::create([
            'code' => '01',        
            'name' => 'Mercaderias',
        ]        
        );
        App\ProductExistence::create([
            'code' => '02',        
            'name' => 'Productos terminados',
        ]        
        );
        App\ProductExistence::create([
            'code' => '03',        
            'name' => 'Materias primas',
        ]        
        );
        App\ProductExistence::create([
            'code' => '04',        
            'name' => 'Envases',
        ]        
        );
        App\ProductExistence::create([
            'code' => '05',        
            'name' => 'Materiales auxiliares',
        ]        
        );
        App\ProductExistence::create([
            'code' => '06',        
            'name' => 'Suministros',
        ]        
        );
        App\ProductExistence::create([
            'code' => '07',        
            'name' => 'Repuestos',
        ]        
        );
        App\ProductExistence::create([
            'code' => '08',        
            'name' => 'Embalajes',
        ]        
        );
        App\ProductExistence::create([
            'code' => '09',        
            'name' => 'Subproductos',
        ]        
        );
        App\ProductExistence::create([
            'code' => '10',        
            'name' => 'Desechos y desperdicios',
        ]        
        );
        App\ProductExistence::create([
            'code' => '91',        
            'name' => 'Otros 1',
        ]        
        );
        App\ProductExistence::create([
            'code' => '92',        
            'name' => 'Otros 2',
        ]        
        );
        App\ProductExistence::create([
            'code' => '93',        
            'name' => 'Otros 3',
        ]        
        );
        App\ProductExistence::create([
            'code' => '94',        
            'name' => 'Otros 4',
        ]        
        );
        App\ProductExistence::create([
            'code' => '95',        
            'name' => 'Otros 5',
        ]        
        );
        App\ProductExistence::create([
            'code' => '96',        
            'name' => 'Otros 6',
        ]        
        );
        App\ProductExistence::create([
            'code' => '97',        
            'name' => 'Otros 7',
        ]        
        );
        App\ProductExistence::create([
            'code' => '98',        
            'name' => 'Otros 8',
        ]        
        );
        App\ProductExistence::create([
            'code' => '99',        
            'name' => 'Otros',
        ]        
        );




        //Identification
        App\Identification::create([
            'code' => '0',           
            'name' => 'Otros',
        ]        
        );
        App\Identification::create([
            'code' => '1',           
            'name' => 'DOCUMENTO NACIONAL DE IDENTIDAD(DNI)',
        ]        
        );
        App\Identification::create([
            'code' => '4',           
            'name' => 'CARNET DE EXTRANJERIA(CE)',
        ]        
        );
        App\Identification::create([
            'code' => '6',           
            'name' => 'REGISTRO ÚNICO DE CONTRIBUYENTES(RUC)',
        ]        
        );
        App\Identification::create([
            'code' => '7',           
            'name' => 'PASAPORTE(PP)',
        ]        
        );
        App\Identification::create([
            'code' => 'A',           
            'name' => 'CÉDULA DIPLOMÁTICA DE IDENTIDAD(CDI)',
        ]        
        );



    }
}
