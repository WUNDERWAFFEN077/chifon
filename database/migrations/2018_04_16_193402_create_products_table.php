<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->string('reference_code',20)->nullable();
            $table->string('description',200)->nullable();
            $table->text('long_description')->nullable();
            $table->tinyInteger('is_inventariable');
            $table->enum('tax_type',['10','20','30']);
            $table->string('status',20);

            //measure_id (FK)
            $table->string('measure_code',2);
            $table->foreign('measure_code')->references('code')->on('product_measures');

            //category_id (FK)
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('product_categories');

            //line_id (FK)
            $table->integer('line_id')->unsigned();
            $table->foreign('line_id')->references('id')->on('product_lines');

            //existence_code (FK)
            $table->string('existence_code',2);
            $table->foreign('existence_code')->references('code')->on('product_existences');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
