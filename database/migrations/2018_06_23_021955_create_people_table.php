<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identification',15);
            $table->string('full_name',100);
            $table->string('primary_last_name',50);
            $table->string('second_last_name',50);
            $table->string('address',100);
            $table->string('ubigeo_code',6);
            $table->string('gender',1);
            $table->string('email',50);
            $table->string('phone1',20);
            $table->string('phone2',20);
            $table->string('fax',20);
            $table->string('mobile',20);
            $table->text('observations');

            //identification_code (FK)
            $table->string('identification_code',1);
            $table->foreign('identification_code')->references('code')->on('identifications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
