<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->date('issue_date');
            $table->date('due_date');

            //people_id (FK)
            $table->integer('people_id')->unsigned();
            $table->foreign('people_id')->references('id')->on('people');

            //invoice_types (FK)
            $table->string('type_code',2);
            $table->foreign('type_code')->references('code')->on('invoice_types');

            //invoice_series (FK)
            $table->string('serie_code',4);
            $table->foreign('serie_code')->references('code')->on('invoice_series');

            //invoice_transactions (FK)
            $table->string('transaction_code',2);
            $table->foreign('transaction_code')->references('code')->on('invoice_transactions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
