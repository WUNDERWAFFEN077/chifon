<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    //mensajes
	public static $messages = [
		'identification.required' => 'El nro. de identificación es requerido.',
		'full_name.required' => 'El nombre es requerido.',
		'full_name.min' => 'El nombre debe tener al menos 3 caracteres.',
		'description.max' => 'La descripcion corta solo admite hasta 200 caracteres.',
	
	];
	//validar
	public static $rules = [
		'identification' => 'required|min:8',
		'full_name' => 'required|min:5',
		/*'description' => 'max:200',*/
	];
}
