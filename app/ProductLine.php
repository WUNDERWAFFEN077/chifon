<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLine extends Model
{
    // line->brand
    public function product_line(){
    	return $this->belongsTo(ProductBrand::class);
    }
}
