<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //mensajes
	public static $messages = [
		'name.required' => 'El nombre es requerido.',
	
	];
	//validar
	public static $rules = [
		'name' => 'required',
	];
}
