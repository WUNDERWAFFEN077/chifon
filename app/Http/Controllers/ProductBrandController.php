<?php

namespace App\Http\Controllers;

use App\ProductBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};

class ProductBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = ProductBrand::paginate();

        return view('brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new ProductBrand();
        $brand->name = $request->input("name");
        $brand->save();
       
        return redirect('/brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductBrand  $productBrand
     * @return \Illuminate\Http\Response
     */
    public function show(ProductBrand $productBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductBrand  $productBrand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productBrand = ProductBrand::find($id);
        return view('brands.edit')->with(compact('productBrand'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductBrand  $productBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = ProductBrand::find($id);
        $brand->name = $request->input('name');
        $brand->save();   //UPDATE

        return redirect('/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductBrand  $productBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $brand = ProductBrand::find($id);
        
        if ($request->ajax()) {
            $brand->delete();   //DELETE
            return response(['msg' => 'Eliminado correctamente.', 'status' => 'success']);
        }
        return response(['msg' => 'Error en la operación', 'status' => 'failed']);
    }

    public function listGrid() { 
        
       return $this->listarDT();
    }

    //USO DE JGRID
    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT id, name
                    FROM product_brands
                    WHERE name LIKE '%$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT COUNT(*) as t 
                    FROM product_brands
                    WHERE name LIKE '%$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
