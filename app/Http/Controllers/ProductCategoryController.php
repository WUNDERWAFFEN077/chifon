<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};
class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::paginate();

        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new ProductCategory();
        $category->name = $request->input("name");
        $category->save();

        //$users = User::paginate();

        //return view('users.index', compact('users'));
        return redirect('/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //print_r($productCategory);
        $productCategory = ProductCategory::find($id);
        return view('categories.edit')->with(compact('productCategory'));    //formulario de registro
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategory::find($id);
        $category->name = $request->input('name');
        $category->save();   //UPDATE

        return redirect('/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $category = ProductCategory::find($id);
        
        if ($request->ajax()) {
            $category->delete();   //DELETE
            return response(['msg' => 'Eliminado correctamente.', 'status' => 'success']);
        }
        return response(['msg' => 'Error en la operación', 'status' => 'failed']);
    }

   


    public function listGrid() { 
       //return json_encode("PAVAO");
        
       return $this->listarDT();
    }



    //USO DE JGRID
    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT id, name
                    FROM product_categories
                    WHERE name LIKE '%$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT COUNT(*) as t 
                    FROM product_categories
                    WHERE name LIKE '%$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }

    public function listar() : string {
        $dt = new JqGridHelper;
        $q = $dt->q;
        
        try {
            $sql = "SELECT id, name
                    FROM product_categories
                    WHERE name LIKE '%$q%'";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden":"";
            $sql .= " LIMIT  $dt->pagina, $dt->limite";
                    
                
            $result = DB::select($sql);
            

            $sql = "SELECT COUNT(*) as t 
                    FROM product_categories
                    WHERE name LIKE '%$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
