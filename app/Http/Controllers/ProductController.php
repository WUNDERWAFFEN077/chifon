<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductExistence;
use App\ProductCategory;
use App\ProductLine;
use App\ProductMeasure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};

class ProductController extends Controller
{
    private $titulo = "Productos";
    private $titulo2 = "Producto";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $titulo = $this->titulo;
        return view('products.index', compact('titulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;      
        $existences = ProductExistence::orderBy('code')->get();
        $categories = ProductCategory::orderBy('name')->get();
        $lines = ProductLine::orderBy('name')->get();
        $measures = ProductMeasure::orderBy('name')->get();
        $product = new Product();
        return view('products.create')
        ->with(compact('titulo','titulo2','existences','categories','lines','measures','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Product::$rules, Product::$messages);

        $product = new Product();
        $product->name = $request->input("name");
        $product->reference_code = $request->input('reference_code');
        $product->description = $request->input('description');
        $product->long_description = $request->input('long_description');

        if ($request->has('is_inventariable')) {
            $product->is_inventariable = 1;
        }else{
            $product->is_inventariable = 0;
        }
        
        $product->measure_code = $request->input('measure_code');
        $product->category_id = $request->input('category_id');
        $product->line_id = $request->input('line_id');
        $product->existence_code = $request->input('existence_code');
        $product->tax_type = $request->input('tax_type');
        $product->status = 'visible';
        $product->save();
       
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;

        $product = Product::find($id);
        $existences = ProductExistence::orderBy('code')->get();
        $categories = ProductCategory::orderBy('name')->get();
        $lines = ProductLine::orderBy('name')->get();
        $measures = ProductMeasure::orderBy('name')->get();
        
        return view('products.edit')
        ->with(compact('titulo','titulo2','existences','categories','lines','measures','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Product::$rules, Product::$messages);

        $product = Product::find($id);
        $product->name = $request->input("name");
        $product->reference_code = $request->input('reference_code');
        $product->description = $request->input('description');
        $product->long_description = $request->input('long_description');

        if ($request->has('is_inventariable')) {
            $product->is_inventariable = 1;
        }else{
            $product->is_inventariable = 0;
        }
        
        $product->measure_code = $request->input('measure_code');
        $product->category_id = $request->input('category_id');
        $product->line_id = $request->input('line_id');
        $product->existence_code = $request->input('existence_code');
        $product->tax_type = $request->input('tax_type');
        $product->status = 'visible';
        $product->save();
       
        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function listGrid() { 
        
       return $this->listarDT();
    }

    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT A.id,A.name,A.reference_code,A.status,
            B.name AS category,
            C.name AS line,
            D.reference_code AS measure,
            E.name AS existence
            FROM products A
            INNER JOIN product_categories B ON (A.category_id = B.id)
            INNER JOIN product_lines C ON (A.line_id = C.id)
            INNER JOIN product_measures D ON (A.measure_code = D.code)
            INNER JOIN product_existences E ON (A.existence_code= E.code)
            WHERE A.name LIKE '$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT count(*) as t                  
            FROM products A
            INNER JOIN product_categories B ON (A.category_id = B.id)
            INNER JOIN product_lines C ON (A.line_id = C.id)
            INNER JOIN product_measures D ON (A.measure_code = D.code)
            INNER JOIN product_existences E ON (A.existence_code= E.code)
            WHERE A.name LIKE '$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
