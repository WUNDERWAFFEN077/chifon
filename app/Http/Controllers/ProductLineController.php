<?php

namespace App\Http\Controllers;

use App\ProductLine;
use App\ProductBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};

class ProductLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $titulo = "Lineas";
    private $titulo2 = "Linea";

    public function index()
    {
        $lines = ProductLine::paginate();
        $titulo = $this->titulo;

        return view('lines.index', compact('lines','titulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;
        $brands = ProductBrand::orderBy('name')->get();
        $productLine = new ProductLine();
        return view('lines.create')->with(compact('titulo','titulo2','brands','productLine'));;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $line = new ProductLine();
        $line->name = $request->input("name");
        $line->brand_id = $request->input('brand_id');
        $line->save();
       
        return redirect('/lines');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductLine  $productLine
     * @return \Illuminate\Http\Response
     */
    public function show(ProductLine $productLine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductLine  $productLine
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;

        $productLine = ProductLine::find($id);
        $brands = ProductBrand::orderBy('name')->get();
        return view('lines.edit')->with(compact('productLine','titulo','titulo2','brands'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductLine  $productLine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $line = ProductLine::find($id);
        $line->name = $request->input('name');
        $line->brand_id = $request->input('brand_id');
        $line->save();   //UPDATE

        return redirect('/lines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductLine  $productLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductLine $productLine)
    {
        //
    }

    public function listGrid() { 
        
       return $this->listarDT();
    }

    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT A.id,A.name,B.name AS brand
                    FROM product_lines A
                    INNER JOIN product_brands B ON (A.brand_id = B.id)
                    WHERE A.name LIKE '%$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT count(*) as t 
                    FROM product_lines A
                    INNER JOIN product_brands B ON (A.brand_id = B.id)
                    WHERE A.name LIKE '%$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
