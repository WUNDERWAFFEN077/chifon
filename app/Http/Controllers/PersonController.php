<?php

namespace App\Http\Controllers;

use App\Person;
use App\Identification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};

class PersonController extends Controller
{
    private $titulo = "Personas";
    private $titulo2 = "Persona";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $persons = Person::paginate();
        $titulo = $this->titulo;

        return view('persons.index', compact('persons','titulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;      
        $identifications = Identification::orderBy('code')->get();  
        $person = new Person();
        return view('persons.create')->with(compact('titulo','titulo2','identifications','person'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Person::$rules, Person::$messages);

        $person = new Person();
        $person->identification_code = $request->input("identification_code");
        $person->identification = $request->input('identification');
        $person->full_name = $request->input('full_name');
        $person->primary_last_name = $request->input('primary_last_name');
        $person->second_last_name = $request->input('second_last_name');
        $person->address = $request->input('address');
        $person->ubigeo_code = $request->input('ubigeo_code');
        $person->gender = $request->input('gender');
        $person->email = $request->input('email');
        $person->phone1 = $request->input('phone1');
        $person->phone2 = $request->input('phone2');
        $person->fax = $request->input('fax');
        $person->mobile = $request->input('mobile');
        $person->observations = $request->input('observations');
        $person->save();
       
        return redirect('/persons');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;

        $person = Person::find($id);
        $identifications = Identification::orderBy('code')->get(); 
        
        return view('persons.edit')->with(compact('titulo','titulo2','identifications','person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Person::$rules, Person::$messages);

        $person = Person::find($id);
        $person->identification_code = $request->input("identification_code");
        $person->identification = $request->input('identification');
        $person->full_name = $request->input('full_name');
        $person->primary_last_name = $request->input('primary_last_name');
        $person->second_last_name = $request->input('second_last_name');
        $person->address = $request->input('address');
        $person->ubigeo_code = $request->input('ubigeo_code');
        $person->gender = $request->input('gender');
        $person->email = $request->input('email');
        $person->phone1 = $request->input('phone1');
        $person->phone2 = $request->input('phone2');
        $person->fax = $request->input('fax');
        $person->mobile = $request->input('mobile');
        $person->observations = $request->input('observations');
        $person->save();
       
        return redirect('/persons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        //
    }

    public function listGrid() { 
        
       return $this->listarDT();
    }

    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT A.id,
            TRIM(CONCAT(A.primary_last_name,' ',A.second_last_name,' ',A.full_name)) AS full_name,
            identification,
            address
            FROM people A
            INNER JOIN identifications B ON (A.identification_code = B.code)
            WHERE TRIM(CONCAT(primary_last_name,' ',second_last_name,' ',A.full_name,' ',primary_last_name)) LIKE '$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT count(*) as t 
                    FROM people A
                    INNER JOIN identifications B ON (A.identification_code = B.code)
                    WHERE TRIM(CONCAT(primary_last_name,' ',second_last_name,' ',A.full_name,' ',primary_last_name)) LIKE '$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
