<?php

namespace App\Http\Controllers;

use App\ProductMeasure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB AS DB;
use App\Http\Helpers\{JqGridHelper, DataTableHelper};

class ProductMeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measures = ProductMeasure::paginate();

        return view('measures.index', compact('measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('measures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $measure = new ProductMeasure();
        $measure->name = $request->input("name");
        $measure->description = $request->input("description");
        $measure->save();
       
        return redirect('/measures');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductMeasure  $productMeasure
     * @return \Illuminate\Http\Response
     */
    public function show(ProductMeasure $productMeasure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductMeasure  $productMeasure
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productMeasure = ProductMeasure::find($id);
        return view('measures.edit')->with(compact('productMeasure'));    //formulario de registro
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductMeasure  $productMeasure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $measure = ProductMeasure::find($id);
        $measure->name = $request->input('name');
        $measure->description = $request->input("description");
        $measure->save();   //UPDATE

        return redirect('/measures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductMeasure  $productMeasure
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $measure = ProductMeasure::find($id);
        
        if ($request->ajax()) {
            $measure->delete();   //DELETE
            return response(['msg' => 'Eliminado correctamente.', 'status' => 'success']);
        }
        return response(['msg' => 'Error en la operación', 'status' => 'failed']);
    }
    

    public function listGrid() { 
        
       return $this->listarDT();
    }



    //USO DE JGRID
    public function listarDT() : string {
        $dt = new DataTableHelper;
        //$limite = $_REQUEST['draw'];        
        //return json_encode($dt);
        $q = $_REQUEST['q'];   
        try {
            $sql = "SELECT code, reference_code, name
                    FROM product_measures
                    WHERE name LIKE '%$q%' ";
            $sql .= ($dt->columna!="")?" ORDER BY $dt->columna $dt->columna_orden ":"";
            $sql .= " LIMIT $dt->pagina, $dt->limite";
            

                
            $result = DB::select($sql);
            

            $sql = "SELECT COUNT(*) as t 
                    FROM product_measures
                    WHERE name LIKE '%$q%'
            ";
            $total = DB::select($sql);

            

            return $dt->response(
                $result,
                $total[0]->t
            );

            
        } catch (Exception $e) {
            //Log::error('Error', $e->getMessage());
        }

        return "";
    }
}
