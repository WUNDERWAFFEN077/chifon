<?php

namespace App\Http\Controllers;

use App\ProductExistence;
use Illuminate\Http\Request;

class ProductExistenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductExistence  $productExistence
     * @return \Illuminate\Http\Response
     */
    public function show(ProductExistence $productExistence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductExistence  $productExistence
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductExistence $productExistence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductExistence  $productExistence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductExistence $productExistence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductExistence  $productExistence
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductExistence $productExistence)
    {
        //
    }
}
