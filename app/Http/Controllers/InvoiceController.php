<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceProduct;
use App\Person;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    private $titulo = "Facturas";
    private $titulo2 = "Factura";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = $this->titulo;
        return view('invoices.index', compact('titulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = $this->titulo;
        $titulo2 = $this->titulo2;
        return view('invoices.create')
        ->with(compact('titulo','titulo2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('invoices');
        
        try {
            $invoice = new Invoice();
            $invoice->people_id =  $request->people_id;
            $invoice->issue_date =  $request->issue_date;
            $invoice->due_date =  $request->due_date;
            $invoice->save();

            $detalles = $request->data;//Array de detalles

            foreach($detalles as $ep=>$det)
            {
                $detalle = new InvoiceProduct();
                $detalle->invoice_id = $invoice->id;
                $detalle->product_id = $det['product_id'];
                $detalle->quantity = $det['quantity'];
                $detalle->price_amount = $det['price_amount'];
                $detalle->total_amount = $det['total_amount'];         
                $detalle->save();
            }   

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return 'lol';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function selectpeople(Request $request){
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $persons = Person::where('full_name','like',"%$term%")
                        ->orderBy('full_name')
                        ->get();

        $formatted_tags = [];

        foreach ($persons as $person) {
            $formatted_tags[] = ['id' => $person->id, 
            'text' => trim($person->full_name.' '.$person->primary_last_name.' '.$person->second_last_name)];
        }
        /*
        return \Response::json($formatted_tags);*/
        //if ($request->ajax()) {
                     

        //*return response()->json($formatted_tags);
        return \Response::json($formatted_tags);
        //}        

    }

    public function selectproduct(Request $request){
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $products = Product::where('name','like',"%$term%")
                        ->orderBy('name')
                        ->get();

        $formatted_tags = [];

        foreach ($products as $product) {
            $formatted_tags[] = ['id' => $product->id, 
            'text' => trim($product->name)];
        }
        /*
        return \Response::json($formatted_tags);*/
        //if ($request->ajax()) {
                     

        //*return response()->json($formatted_tags);
        return \Response::json($formatted_tags);
        //}        

    }
}

