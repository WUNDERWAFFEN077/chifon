<?php
namespace App\Http\Helpers;

class JqGridHelper
{
    public $limite = 0;
    public $pagina = 0;
    public $columna = '';
    public $columna_orden = '';
    public $filtros = [];
    public $parametros = [];
        
    public $q = '';

    public function __CONSTRUCT()
    {
        /* Cantidad de registros por pÃ¡gina */
        $this->limite = $_REQUEST['rows'];
        if(!is_numeric($this->limite)) return;
        
        /* Desde que numero de fila va a paginar */
        $this->pagina = $_REQUEST['page'] - 1;
        if(!is_numeric($this->pagina)) return;
        
        if( $this->pagina > 0) $this->pagina = $this->pagina * $this->limite;
        
        /* Ordenamiento de las filas */
        $this->columna = $_REQUEST['sidx'];
        $this->columna_orden = $_REQUEST['sord'];

        if(isset($_REQUEST['dato']))
            $this->q = rawurldecode($_REQUEST['dato']);
      

        /* Parametros adicionales */
        if(isset($_REQUEST['parametros']))
            $this->parametros = json_decode($_REQUEST['parametros'], true);
        

    }
    
    public function response($data, $count)
    {
        
        if( $count >0 ){ $total_pages = ceil($count/$this->limite); }
        else { $total_pages = 0; }
        
        
        return json_encode(array(
            'rows' => $data,
            'total' => $total_pages,
            'records' => $count
        ));
    }
}