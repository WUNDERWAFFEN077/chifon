<?php
namespace App\Http\Helpers;

class DataTableHelper
{
    public $limite = 0;
    public $pagina = 0;
    public $columna = '';
    public $columna_orden = '';
    public $filtros = [];
    public $parametros = [];
        
    public $q = '';

    public function __CONSTRUCT()
    {   
        //Cantidad de registros por pÃ¡gina
        $this->limite = $_REQUEST['length'];
        if(!is_numeric($this->limite)) return;
        
        // Desde que numero de fila va a paginar
        //$this->pagina = $_REQUEST['start'] - 1;
        $this->pagina = $_REQUEST['start'];
        if(!is_numeric($this->pagina)) return;


        /*$columns = array(
            0 => 'id',
            1 => 'name'
        );*/
        
        //if( $this->pagina > 0) $this->pagina = $this->pagina * $this->limite;
        
        // Ordenamiento de las filas
        
        //$this->columna = $columns[$_REQUEST['order'][0]['column']];
        $this->columna = $_REQUEST['order'][0]['column'] + 1;
        $this->columna_orden = $_REQUEST['order'][0]['dir'];



        /*if(isset($_REQUEST['dato']))
            $this->q = rawurldecode($_REQUEST['dato']);*/
      

        /* Parametros adicionales */
        /*if(isset($_REQUEST['parametros']))
            $this->parametros = json_decode($_REQUEST['parametros'], true);*/

        
        

    }
    
    public function response($data, $count)
    {
        
        if( $count >0 ){ $total_pages = ceil($count/$this->limite); }
        else { $total_pages = 0; }
        
        
        return json_encode(array(            
            "draw" => intval($_REQUEST['draw']),            
            "recordsTotal" => intval($count),
            "recordsFiltered" => intval($count),
            "data" => $data
        ));
    }
}