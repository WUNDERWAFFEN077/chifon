<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('invoices/selectpeople', 'InvoiceController@selectpeople');

Route::middleware(['auth'])->group(function () {
	//Roles
	Route::post('roles/store', 'RoleController@store')->name('roles.store')
		->middleware('permission:roles.create');

	Route::get('roles', 'RoleController@index')->name('roles.index')
		->middleware('permission:roles.index');

	Route::get('roles/create', 'RoleController@create')->name('roles.create')
		->middleware('permission:roles.create');

	Route::put('roles/{role}', 'RoleController@update')->name('roles.update')
		->middleware('permission:roles.edit');

	Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
		->middleware('permission:roles.show');

	Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
		->middleware('permission:roles.destroy');

	Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
		->middleware('permission:roles.edit');

	//Users
	Route::get('users', 'UserController@index')->name('users.index')
		->middleware('permission:users.index');

	Route::get('users/create', 'UserController@create')->name('users.create')
		->middleware('permission:users.create');

	Route::post('users/store', 'UserController@store')->name('users.store')
		->middleware('permission:users.create');

	Route::put('users/{user}', 'UserController@update')->name('users.update')
		->middleware('permission:users.edit');

	Route::get('users/{user}', 'UserController@show')->name('users.show')
		->middleware('permission:users.show');

	Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
		->middleware('permission:users.destroy');

	Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
		->middleware('permission:users.edit');


	//Category
	Route::get('categories', 'ProductCategoryController@index')->name('categories.index')
		->middleware('permission:categories.index');

	Route::get('categories/create', 'ProductCategoryController@create')->name('categories.create')
		->middleware('permission:categories.create');

	Route::post('categories/store', 'ProductCategoryController@store')->name('categories.store')
		->middleware('permission:categories.create');	

	Route::put('categories/{category}', 'ProductCategoryController@update')->name('categories.update')
		->middleware('permission:categories.edit');

	Route::get('categories/{category}', 'ProductCategoryController@show')->name('categories.show')
		->middleware('permission:categories.show');

	Route::delete('categories/{category}', 'ProductCategoryController@destroy')->name('categories.destroy')
		->middleware('permission:categories.destroy');

	Route::get('categories/{category}/edit', 'ProductCategoryController@edit')->name('categories.edit')
		->middleware('permission:categories.edit');

	Route::post('categories/listGrid', 'ProductCategoryController@listGrid');

	//Measures
	Route::get('measures', 'ProductMeasureController@index')->name('measures.index')
		->middleware('permission:measures.index');

	Route::get('measures/create', 'ProductMeasureController@create')->name('measures.create')
		->middleware('permission:measures.create');

	Route::post('measures/store', 'ProductMeasureController@store')->name('measures.store')
		->middleware('permission:measures.create');	

	Route::put('measures/{category}', 'ProductMeasureController@update')->name('measures.update')
		->middleware('permission:measures.edit');

	Route::get('measures/{category}', 'ProductMeasureController@show')->name('measures.show')
		->middleware('permission:measures.show');

	Route::delete('measures/{category}', 'ProductMeasureController@destroy')->name('measures.destroy')
		->middleware('permission:measures.destroy');

	Route::get('measures/{category}/edit', 'ProductMeasureController@edit')->name('measures.edit')
		->middleware('permission:measures.edit');

	Route::post('measures/listGrid', 'ProductMeasureController@listGrid');

	//Brands
	Route::get('brands', 'ProductBrandController@index')->name('brands.index')
		->middleware('permission:brands.index');

	Route::get('brands/create', 'ProductBrandController@create')->name('brands.create')
		->middleware('permission:brands.create');

	Route::post('brands/store', 'ProductBrandController@store')->name('brands.store')
		->middleware('permission:brands.create');	

	Route::put('brands/{category}', 'ProductBrandController@update')->name('brands.update')
		->middleware('permission:brands.edit');

	Route::get('brands/{category}', 'ProductBrandController@show')->name('brands.show')
		->middleware('permission:brands.show');

	Route::delete('brands/{category}', 'ProductBrandController@destroy')->name('brands.destroy')
		->middleware('permission:brands.destroy');

	Route::get('brands/{category}/edit', 'ProductBrandController@edit')->name('brands.edit')
		->middleware('permission:brands.edit');

	Route::post('brands/listGrid', 'ProductBrandController@listGrid');


	//Lines
	Route::get('lines', 'ProductLineController@index')->name('lines.index')
		->middleware('permission:lines.index');

	Route::get('lines/create', 'ProductLineController@create')->name('lines.create')
		->middleware('permission:lines.create');

	Route::post('lines/store', 'ProductLineController@store')->name('lines.store')
		->middleware('permission:lines.create');	

	Route::put('lines/{category}', 'ProductLineController@update')->name('lines.update')
		->middleware('permission:lines.edit');

	Route::get('lines/{category}', 'ProductLineController@show')->name('lines.show')
		->middleware('permission:lines.show');

	Route::delete('lines/{category}', 'ProductLineController@destroy')->name('lines.destroy')
		->middleware('permission:lines.destroy');

	Route::get('lines/{category}/edit', 'ProductLineController@edit')->name('lines.edit')
		->middleware('permission:lines.edit');

	Route::post('lines/listGrid', 'ProductLineController@listGrid');


	//Products
	Route::get('products', 'ProductController@index')->name('products.index')
		->middleware('permission:products.index');

	Route::post('products/store', 'ProductController@store')->name('products.store')
		->middleware('permission:products.create');	

	Route::get('products/create', 'ProductController@create')->name('products.create')
		->middleware('permission:products.create');

	Route::put('products/{product}', 'ProductController@update')->name('products.update')
		->middleware('permission:products.edit');

	Route::get('products/{product}', 'ProductController@show')->name('products.show')
		->middleware('permission:products.show');

	Route::delete('products/{product}', 'ProductController@destroy')->name('products.destroy')
		->middleware('permission:products.destroy');

	Route::get('products/{product}/edit', 'ProductController@edit')->name('products.edit')
		->middleware('permission:products.edit');
	Route::post('products/listGrid', 'ProductController@listGrid');


	//-------------------------------------------------
	//-------------------PERSONAS----------------------
	//-------------------------------------------------
	//Persons
	Route::get('persons', 'PersonController@index')->name('persons.index')
		->middleware('permission:persons.index');
	Route::get('persons/create', 'PersonController@create')->name('persons.create')
		->middleware('permission:persons.create');
	Route::post('persons/store', 'PersonController@store')->name('persons.store')
		->middleware('permission:persons.create');	
	Route::put('persons/{person}', 'PersonController@update')->name('persons.update')
		->middleware('permission:persons.edit');
	Route::get('persons/{person}', 'PersonController@show')->name('persons.show')
		->middleware('permission:persons.show');
	Route::delete('persons/{person}', 'PersonController@destroy')->name('persons.destroy')
		->middleware('permission:persons.destroy');
	Route::get('persons/{person}/edit', 'PersonController@edit')->name('persons.edit')
		->middleware('permission:persons.edit');
	Route::post('persons/listGrid', 'PersonController@listGrid');


	//-------------------------------------------------
	//---------------------VENTAS----------------------
	//-------------------------------------------------
	//Invoices
	Route::get('invoices/selectpeople', 'InvoiceController@selectpeople');
	Route::get('invoices/selectproduct', 'InvoiceController@selectproduct');
	Route::get('invoices', 'InvoiceController@index')->name('invoices.index')
		->middleware('permission:invoices.index');
	Route::get('invoices/create', 'InvoiceController@create')->name('invoices.create')
		->middleware('permission:invoices.create');
	Route::post('invoices/store', 'InvoiceController@store')->name('invoices.store')
		->middleware('permission:invoices.create');	
	Route::put('invoices/{invoice}', 'InvoiceController@update')->name('invoices.update')
		->middleware('permission:invoices.edit');
	Route::get('invoices/{invoice}', 'InvoiceController@show')->name('invoices.show')
		->middleware('permission:invoices.show');
	Route::delete('invoices/{invoice}', 'InvoiceController@destroy')->name('invoices.destroy')
		->middleware('permission:invoices.destroy');
	Route::get('invoices/{invoice}/edit', 'InvoiceController@edit')->name('invoices.edit')
		->middleware('permission:invoices.edit');
	Route::post('invoices/listGrid', 'InvoiceController@listGrid');	

	//Route::get('invoices/selectpeople', 'InvoiceController@selectpeople');
	
});